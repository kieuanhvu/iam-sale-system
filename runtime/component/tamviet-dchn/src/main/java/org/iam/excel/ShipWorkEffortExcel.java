package org.iam.excel;

import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.iam.util.CommonUtils;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.entity.EntityValueNotFoundException;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ShipWorkEffortExcel {
    protected ExecutionContextImpl eci;
    private String location;
    private String workEffortId;
    private String geoId;
    public ShipWorkEffortExcel(ExecutionContextImpl eci, String location, String workEffortId, String geoId){
        this.eci = eci;
        this.location = location;
        this.workEffortId = workEffortId;
        this.geoId = geoId;
    }

    public XSSFWorkbook exportData() throws IOException {
        int itemDataRowStart = 16;
        int currentRowInsert = itemDataRowStart;
        ResourceReference rr = eci.getResource().getLocationReference(location);
        String rootPartyId = System.getProperty("rootPartyId");
        EntityValue rootParty = eci.getEntity().find("mantle.party.Organization").condition("partyId", rootPartyId).one();
        EntityList wefList = eci.getEntity().find("iam.shipment.ShipmentWorkEffortOrder")
                .condition("workEffortId", workEffortId).list();
        if(wefList == null || wefList.size() == 0){
            throw new EntityValueNotFoundException(CommonUtils.encodeString("Không tìm thấy phiếu xuất"));
        }
        EntityValue wef = wefList.getFirst();
        EntityList orderPartList = eci.getEntity().find("mantle.order.OrderPart").condition("orderId", wef.get("orderId")).list();
        EntityList orderPartPosContactMech = eci.getEntity().find("iam.order.contact.OrderPartContactMechPostalAddr")
                .condition("orderId", wef.get("orderId"))
                .condition("contactMechPurposeId", "PostalPrimary").list();

        EntityList orderPartTelecomNumberMech = eci.getEntity().find("iam.order.contact.OrderPartContactMechTelecomNumber")
                .condition("orderId", wef.get("orderId"))
                .condition("contactMechPurposeId", "PhonePrimary").list();

        EntityValue postalAddress = eci.getEntity().find("mantle.party.contact.PostalAddress").condition("contactMechId", orderPartList.getFirst().get("postalContactMechId")).one();
        EntityValue telecomNumber = eci.getEntity().find("mantle.party.contact.TelecomNumber").condition("contactMechId", orderPartList.getFirst().get("telecomContactMechId")).one();
        String shippingAddress = postalAddress != null? postalAddress.getString("directions") : "";
        String shippingContactNumber = telecomNumber != null? telecomNumber.getString("contactNumber") : "";

        String customerAddress = (orderPartPosContactMech != null && orderPartPosContactMech.size() > 0)? orderPartPosContactMech.getFirst().getString("directions") : "";
        String customerContactNumber = (orderPartTelecomNumberMech != null && orderPartTelecomNumberMech.size() > 0)? orderPartTelecomNumberMech.getFirst().getString("contactNumber") : "";

        Map deliveryInfoOut = eci.getService().sync().name("org.dchn.IamOutgoingShipmentServices.get#WorkEffortDeliveryInfo").parameter("workEffortId", this.workEffortId).call();

        Map shipmentItemOut = eci.getService().sync().name("org.dchn.IamShipmentServices.get#WorkEffortShipmentItems")
                .parameter("workEffortId", workEffortId)
                .parameter("orderId", wef.get("orderId")).call();
        List<Map> shipmentItems = (List)shipmentItemOut.get("listData");
        Calendar nowCal = eci.getUser().getNowCalendar();
        /*String nowDateTimeStr = CommonUtils.encodeString("Ngày ") + eci.getL10n().format(nowCal.get(Calendar.DATE), "00") + CommonUtils.encodeString(" tháng ")
                + eci.getL10n().format(nowCal.get(Calendar.MONTH) + 1, "00") + CommonUtils.encodeString(" năm ") + nowCal.get(Calendar.YEAR);*/
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet1 = wb.getSheetAt(0);
        //Tên đơn vị
        //setCellValue(sheet1, 0, 2, rootParty.getString("organizationName"));
        //ngay..thang...nam
        //setCellValue(sheet1, 5, 0, nowDateTimeStr);
        //Số DO
        setCellValue(sheet1, 6, 0, CommonUtils.encodeString("Số: ") + wef.getString("universalId"));
        //Ngày xuất kho
        //String issueDateFormat = wef.getTimestamp("actualCompletionDate") != null ? eci.getL10n().format(wef.getTimestamp("actualCompletionDate"), "dd-MM-yyyy") : CommonUtils.encodeString("Chưa xuất kho");
        //setCellValue(sheet1, 8, 3, issueDateFormat);
        //Tên khach hang
        setCellValue(sheet1, 8, 2, wef.getString("customerName"));
        //dia chi KH
        setCellValue(sheet1, 9, 2, customerAddress);
        //So dien thoai KH
        setCellValue(sheet1, 10, 2, customerContactNumber);

        //dia chi nguoi nhan
        setCellValue(sheet1, 11, 2, shippingAddress);

        //SDT nguoi nhan
        setCellValue(sheet1, 12, 2, shippingContactNumber);

        //Ngay giao hang
        setCellValue(sheet1, 8, 9, eci.getL10n().format(wef.get("estimatedCompletionDate"), "dd/MM/yyyy"));
        //Thong tin lai xe
        if(deliveryInfoOut.get("driverPartyName") != null){
            String driverInfo = deliveryInfoOut.get("driverPartyName").toString();
            if(deliveryInfoOut.get("driverTelecomNumber") != null){
                driverInfo += "(" + deliveryInfoOut.get("driverTelecomNumber")  + ")";
            }
            setCellValue(sheet1, 9, 8, driverInfo);
        }
        //Bien kiem soat
        if(deliveryInfoOut.get("deliverableName") != null){
            String deliverableInfo = (String)deliveryInfoOut.get("deliverableName");
            if(deliveryInfoOut.get("deliverableDesc") != null){
                deliverableInfo += "(" + deliveryInfoOut.get("deliverableDesc")  + ")";
            }
            setCellValue(sheet1, 10, 9, deliverableInfo);
        }
        BigDecimal totalQuantity = BigDecimal.ZERO;
        //danh sách sản phẩm trong phiếu xuất
        if(shipmentItems != null){
            int index = 1;
            List<String> productIdList = shipmentItems.stream().map(e -> (String)e.get("productId")).collect(Collectors.toList());
            Map productOut = eci.getService().sync().name("org.dchn.IamProductServices.get#Products").parameter("productIdList", productIdList).call();
            List<Map> productDataList = (List)productOut.get("listData");
            Set productCategorySet = productDataList.stream().map(e -> (String)e.get("productCategoryId")).collect(Collectors.toSet());
            Set categoryIdBrowsedSet = new HashSet();//cac danh muc da duyet qua va tao XSSFRow
            int totalInsertRow = productCategorySet.size() + shipmentItems.size();
            sheet1.shiftRows(itemDataRowStart + 1, sheet1.getLastRowNum(), totalInsertRow - 1);
            XSSFRow rowItemData = sheet1.getRow(itemDataRowStart);
            for(Map productData: productDataList){
                String productCategoryId = (String)productData.get("productCategoryId");
                if(!categoryIdBrowsedSet.contains(productCategoryId)){
                    if(currentRowInsert != itemDataRowStart){
                        cloneItemRow(wb, sheet1, rowItemData, currentRowInsert);
                    }
                    sheet1.addMergedRegion(new CellRangeAddress(currentRowInsert, currentRowInsert, 0, 9));
                    XSSFCell catCell = sheet1.getRow(currentRowInsert).getCell(0);
                    XSSFCellStyle cellStyle = catCell.getCellStyle();
                    if(cellStyle == null){
                        cellStyle = wb.createCellStyle();
                    }
                    cellStyle.setAlignment(HorizontalAlignment.LEFT);
                    XSSFFont font = wb.createFont();
                    font.setBold(true);
                    //cellStyle.setFont(font);
                    setCellValue(sheet1, currentRowInsert, 0, (String)productData.get("categoryName"));
                    categoryIdBrowsedSet.add(productCategoryId);
                    currentRowInsert++;
                }
                List<Map> subShipmentItems = shipmentItems.stream().filter(e -> e.get("productId").equals(productData.get("productId"))).collect(Collectors.toList());
                for(Map item: subShipmentItems) {
                    XSSFRow cloneRow = cloneItemRow(wb, sheet1, rowItemData, currentRowInsert);
                    sheet1.addMergedRegion(new CellRangeAddress(currentRowInsert, currentRowInsert, 2, 6));
                    BigDecimal itemTotal = ((BigDecimal)item.get("quantity")).multiply((BigDecimal)item.get("unitAmount"));
                    XSSFCell indexCell = cloneRow.getCell(0);
                    XSSFCellStyle cellStyle = indexCell.getCellStyle();
                    if(cellStyle == null){
                        cellStyle = wb.createCellStyle();
                    }
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    XSSFFont font = wb.createFont();
                    font.setBold(false);
                    cellStyle.setFont(font);
                    String productName = (String)item.get("itemDescription");
                    if(geoId != null){
                        EntityValue productGeo = eci.getEntity().find("mantle.product.ProductGeo")
                                .condition("productId", item.get("productId"))
                                .condition("geoId", this.geoId).one();
                        if(productGeo != null && productGeo.getString("description") != null && productGeo.getString("description").trim().length() > 0){
                            productName = productGeo.getString("description");
                        }
                    }
                    totalQuantity = totalQuantity.add(item.get("quantity") != null? (BigDecimal)item.get("quantity"): BigDecimal.ZERO);
                    setCellValue(sheet1, currentRowInsert, 0, String.valueOf(index));
                    setCellValue(sheet1, currentRowInsert, 1, (String)item.get("pseudoId"));
                    setCellValue(sheet1, currentRowInsert, 2, productName);
                    setCellValue(sheet1, currentRowInsert, 7, eci.getL10n().format(item.get("quantity"), "#,##0"));
                    setCellValue(sheet1, currentRowInsert, 8, eci.getL10n().format(item.get("unitAmount"), "#,##0"));
                    setCellValue(sheet1, currentRowInsert, 9, eci.getL10n().format(itemTotal, "#,##0"));
                    currentRowInsert++;
                    index++;
                }//end inner for
            }//end outer for
        }
        int subTotalRow = currentRowInsert;
        int shippingFeeRow = subTotalRow + 2;
        int otherFeeRow = shippingFeeRow + 1;
        int grandTotalRow = otherFeeRow + 1;
        int noteRow = grandTotalRow + 1;
        BigDecimal subTotal = (BigDecimal)wef.get("grandTotal");
        BigDecimal shippingFee = wef.get("shippingFee") != null? (BigDecimal)wef.get("shippingFee") : BigDecimal.ZERO;
        BigDecimal otherFee = wef.get("otherFee") != null? (BigDecimal)wef.get("otherFee") : BigDecimal.ZERO;
        BigDecimal grandTotal = subTotal.add(shippingFee).add(otherFee);
        setCellValue(sheet1, subTotalRow, 9, eci.getL10n().format(subTotal, "#,##0"));
        setCellValue(sheet1, subTotalRow, 7, eci.getL10n().format(totalQuantity, "#,##0"));
        setCellValue(sheet1, shippingFeeRow, 9, eci.getL10n().format(shippingFee, "#,##0"));//phi van chuyen
        setCellValue(sheet1, otherFeeRow, 9, eci.getL10n().format(otherFee, "#,##0"));//chi phi khac
        setCellValue(sheet1, grandTotalRow, 9, eci.getL10n().format(grandTotal, "#,##0"));//tong tien
        setCellValue(sheet1, noteRow, 2, wef.getString("otherFeeDesc") != null? wef.getString("otherFeeDesc"): "");
        return wb;
    }

    private XSSFRow cloneItemRow(XSSFWorkbook wb, XSSFSheet sheet, XSSFRow srcRow, int rowNum){
        XSSFRow copyRow = sheet.createRow(rowNum);
        int fCell = srcRow.getFirstCellNum();
        int lCell = srcRow.getLastCellNum();
        for (int iCell = fCell; iCell < lCell; iCell++) {
            XSSFCell origCell = srcRow.getCell(iCell);
            XSSFCell copyCell = copyRow.createCell(iCell);
            if (origCell != null) {
                XSSFCellStyle origCellStyle = origCell.getCellStyle();
                XSSFCellStyle copyCellStyle = wb.createCellStyle();
                copyCellStyle.cloneStyleFrom(origCellStyle);
                copyCell.setCellStyle(copyCellStyle);
                CellType origCellType = origCell.getCellTypeEnum();
                copyCell.setCellType(origCellType);
                switch (origCellType) {
                    case BLANK:
                        copyCell.setCellValue("");
                        break;
                    case BOOLEAN:
                        copyCell.setCellValue(origCell.getBooleanCellValue());
                        break;
                    case ERROR:
                        copyCell.setCellErrorValue(origCell.getErrorCellValue());
                        break;
                    case NUMERIC:
                        copyCell.setCellValue(origCell.getNumericCellValue());
                        break;
                    case STRING:
                        copyCell.setCellValue(origCell.getStringCellValue());
                        break;
                    default:
                        copyCell.setCellFormula(origCell.getCellFormula());
                }
            }
        }
        return copyRow;
    }

    private void setCellValue(XSSFSheet sheet, int rowNum, int cellNum, String cellValue){
        XSSFRow row = sheet.getRow(rowNum);
        if(row == null){
            return;
        }
        XSSFCell cell = row.getCell(cellNum);
        if(cell == null){
            cell = row.createCell(cellNum);
        }
        cell.setCellValue(cellValue);
    }
}
