package org.iam.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class ExcelUtils {
    private short titleHeaderHeight = 14 * 20;
    private short normalHeight = 11 * 20;
    private String fontName = "Times New Roman";
    public XSSFCellStyle createCellStyle(XSSFWorkbook wb, VerticalAlignment verticalAlignment, HorizontalAlignment horizontalAlignment, boolean isBold, boolean isItalic, short fontHeight,
                                         BorderStyle borderTop, BorderStyle borderLeft, BorderStyle borderBottom, BorderStyle borderRight){
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(horizontalAlignment);
        cellStyle.setVerticalAlignment(verticalAlignment);
        XSSFFont font = wb.createFont();
        font.setBold(isBold);
        font.setItalic(isItalic);
        font.setFontHeight(fontHeight);
        font.setFontName(fontName);
        cellStyle.setFont(font);
        if(borderTop != null){
            cellStyle.setBorderTop(borderTop);
        }
        if(borderLeft != null){
            cellStyle.setBorderLeft(borderLeft);
        }
        if(borderBottom != null){
            cellStyle.setBorderBottom(borderBottom);
        }
        if(borderRight != null){
            cellStyle.setBorderRight(borderRight);
        }
        return cellStyle;
    }

    public XSSFFont getNormalFont(XSSFWorkbook wb){
        XSSFFont font = wb.createFont();
        font.setFontHeight(normalHeight);
        font.setFontName(fontName);
        return font;
    }

    public XSSFFont getBoldFont(XSSFWorkbook wb){
        XSSFFont font = wb.createFont();
        font.setFontHeight(normalHeight);
        font.setFontName(fontName);
        font.setBold(true);
        return font;
    }

    public XSSFCellStyle getTitleHeaderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, true, false, titleHeaderHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getNormalStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, false, false, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getNormalAlignRightStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, false, false, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getNormalBorderAlignRightStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getBoldStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, true, false, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getBoldAlignCenterStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, true, false, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getBoldAlignRightStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, true, false, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getBoldAlignCenterBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, true, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getItalicStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, true, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getItalicLeftStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, false, true, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getItalicAlignLeftBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, false, true, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getItalicAlignRightBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, false, true, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getItalicRightStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, false, true, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getBoldItalicStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, true, true, normalHeight,
                null, null, null, null);
    }

    public XSSFCellStyle getNormalFullBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getNormalCenterFullBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getNormalRightFullBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getNormalTopLeftBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, null, null);
    }

    public XSSFCellStyle getNormalTopLeftBottomBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, null);
    }

    public XSSFCellStyle getNormalBottomRightBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, false, normalHeight,
                null, null, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getNormalLeftBottomRightBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, false, false, normalHeight,
                null, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getBoldFullBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.CENTER, true, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getBoldAlignLeftBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.LEFT, true, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public XSSFCellStyle getBoldAlignRightBorderStyle(XSSFWorkbook wb){
        return this.createCellStyle(wb, VerticalAlignment.CENTER, HorizontalAlignment.RIGHT, true, false, normalHeight,
                BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);
    }

    public void createRowContent(XSSFSheet sheet, int rowNum, int cellNum, XSSFCellStyle cellStyle, Short rowHeight, CellRangeAddress cellAddresses, Object cellValue){
        this.createRowContent(sheet, rowNum, cellNum, cellStyle, CellType.STRING, rowHeight, cellAddresses, cellValue);
    }

    public void createRowContent(XSSFSheet sheet, int rowNum, int cellNum, XSSFCellStyle cellStyle, CellType cellType, Short rowHeight, CellRangeAddress cellAddresses, Object cellValue){
        XSSFRow row = sheet.getRow(rowNum);
        if(row == null){
            row = sheet.createRow(rowNum);
        }
        XSSFCell cell = row.getCell(cellNum);
        if(cell == null){
            cell = row.createCell(cellNum);
        }
        if(rowHeight != null){
            row.setHeight(rowHeight);
        }

        if(cellStyle != null){
            if(cellAddresses != null){
                int firstColumn = cellAddresses.getFirstColumn();
                int lastColumn = cellAddresses.getLastColumn();
                int firstRow = cellAddresses.getFirstRow();
                int lastRow = cellAddresses.getLastRow();
                for(int i = firstRow; i <= lastRow; i++){
                    XSSFRow tempRow = sheet.getRow(i);
                    if(tempRow == null){
                        tempRow = sheet.createRow(i);
                    }
                    for(int j = firstColumn; j <= lastColumn; j++){
                        XSSFCell tempCell = tempRow.getCell(j);
                        if(tempCell == null){
                            tempCell = tempRow.createCell(j);
                        }
                        tempCell.setCellStyle(cellStyle);
                    }
                }

            }else{
                cell.setCellStyle(cellStyle);
            }
        }
        if(cellAddresses != null){
            sheet.addMergedRegion(cellAddresses);
        }
        if(cellType != null){
            cell.setCellType(cellType);
        }
        if(cellValue == null){
            cell.setCellValue("");
        } else if(cellValue instanceof String){
            cell.setCellValue((String)cellValue);
        }else if(cellValue instanceof Date){
            cell.setCellValue((Date)cellValue);
        }else if(cellValue instanceof Double){
            cell.setCellValue((Double)cellValue);
        }else if(cellValue instanceof LocalDate){
            cell.setCellValue((LocalDate)cellValue);
        }else if(cellValue instanceof RichTextString){
            cell.setCellValue((RichTextString)cellValue);
        }else if(cellValue instanceof BigDecimal){
            BigDecimal value = (BigDecimal)cellValue;
            cell.setCellValue(value.doubleValue());
        }else{
            cell.setCellValue(String.valueOf(cellValue));
        }
    }

    public CellRangeAddress createCellAddresses(int firstRow, int lastRow, int firstCol, int lastCol){
        return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
    }

    public XSSFRichTextString createRichTextStr(String text, List<ExcelRichTextInfo> richTextInfos){
        XSSFRichTextString richString = new XSSFRichTextString(text);
        int index = 0;
        for(ExcelRichTextInfo info: richTextInfos){
            String str = info.getText();
            richString.applyFont(index, index + str.length(), info.getFont());
            index += str.length();
        }
        return richString;
    }
}
