package org.iam.excel;

import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.iam.util.CommonUtils;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class CustomerLiabilityExcel {
    protected ExecutionContextImpl eci;
    private String customerPartyId;
    private Integer periodNum;
    private String location;
    private List<String> timePeriodIds;
    private Timestamp startDate;
    private Timestamp endDate;
    public CustomerLiabilityExcel(ExecutionContextImpl eci, String customerPartyId, Integer periodNum, Timestamp startDate, Timestamp endDate, String location){
        this.eci = eci;
        this.customerPartyId = customerPartyId;
        this.periodNum = periodNum;
        this.location = location;
        if(startDate != null){
            this.startDate = CommonUtils.getStartOfDate(eci, startDate);
        }
        if(endDate != null){
            this.endDate = CommonUtils.getEndOfDate(eci, endDate);;
        }
        if(periodNum == null){
            this.periodNum = eci.getUser().getNowCalendar().get(Calendar.YEAR);
        }
    }

    public XSSFWorkbook exportData() throws IOException {
        Map orgInternalOut = eci.getService().sync().name("org.dchn.IamPartyServices.get#OrgInternalFirstLevel").call();
        List<String> organizationPartyIds = ((List<Map>)orgInternalOut.get("listData")).stream().map(e -> (String)e.get("fromPartyId")).collect(Collectors.toList());
        String rootPartyId = System.getProperty("rootPartyId");
        ExcelUtils excelUtils = new ExcelUtils();
        EntityList tpList = eci.getEntity().find("mantle.party.time.TimePeriod")
                .condition("periodNum", this.periodNum)
                .condition("timePeriodTypeId", "FiscalYear")
                .condition("partyId", EntityCondition.IN, rootPartyId)
                .list();
        this.timePeriodIds = tpList.stream().map(e -> (String)e.get("timePeriodId")).collect(Collectors.toList());
        Map periodInfoOut = eci.getService().sync().name("mantle.party.TimeServices.get#TimePeriodInfo")
                .parameter("timePeriodId", this.timePeriodIds.get(0)).call();
        Timestamp startPeriodTs = (Timestamp)periodInfoOut.get("fromTimestamp");
        Timestamp fromDate = null, thruDate = null;
        if(this.startDate != null){
            fromDate = startDate;
        }else{
            fromDate = (Timestamp)periodInfoOut.get("fromTimestamp");
        }
        if(this.endDate != null){
            thruDate = endDate;
        }else{
            thruDate = (Timestamp)periodInfoOut.get("thruTimestamp");
        }

        /*Map customerOverviewOut = eci.getService().sync().name("org.dchn.IamAccountReceiveServices.get#LiabilityCustomerOverview")
                .parameter("customerId", customerPartyId)
                .parameter("periodNum", periodNum)
                .call();
        String customerName = (String)customerOverviewOut.get("customerName");
        String customerPseudoId = (String)customerOverviewOut.get("customerPseudoId");

        BigDecimal beginningBalanceDchn = (BigDecimal)customerOverviewOut.get("beginningBalanceDchn");
        BigDecimal paymentAmountDchn = (BigDecimal)customerOverviewOut.get("paymentAmountDchn");
        BigDecimal debitsAmountDchn = (BigDecimal)customerOverviewOut.get("debitsAmountDchn");
        BigDecimal unpaidAmountDchn = (BigDecimal)customerOverviewOut.get("unpaidAmountDchn");
        BigDecimal debitsReturnAmountDchn = (BigDecimal)customerOverviewOut.get("debitsReturnAmountDchn");
        BigDecimal shippingFeeAmountDchn = (BigDecimal)customerOverviewOut.get("shippingFeeAmountDchn");
        BigDecimal otherFeeAmountDchn = (BigDecimal)customerOverviewOut.get("otherFeeAmountDchn");
        BigDecimal refundAmountTotalDchn = (BigDecimal)customerOverviewOut.get("refundAmountTotalDchn");
        BigDecimal invoiceSaleTotalDchn = (BigDecimal)customerOverviewOut.get("invoiceSaleTotalDchn");

        BigDecimal beginningBalanceFsn = (BigDecimal)customerOverviewOut.get("beginningBalanceFsn");
        BigDecimal paymentAmountFsn = (BigDecimal)customerOverviewOut.get("paymentAmountFsn");
        BigDecimal debitsAmountFsn = (BigDecimal)customerOverviewOut.get("debitsAmountFsn");
        BigDecimal unpaidAmountFsn = (BigDecimal)customerOverviewOut.get("unpaidAmountFsn");
        BigDecimal debitsReturnAmountFsn = (BigDecimal)customerOverviewOut.get("debitsReturnAmountFsn");
        BigDecimal shippingFeeAmountFsn = (BigDecimal)customerOverviewOut.get("shippingFeeAmountFsn");
        BigDecimal otherFeeAmountFsn = (BigDecimal)customerOverviewOut.get("otherFeeAmountFsn");
        BigDecimal refundAmountTotalFsn = (BigDecimal)customerOverviewOut.get("refundAmountTotalFsn");
        BigDecimal invoiceSaleTotalFsn = (BigDecimal)customerOverviewOut.get("invoiceSaleTotalFsn");

        BigDecimal feeTotal = shippingFeeAmountDchn.add(shippingFeeAmountFsn).add(otherFeeAmountDchn).add(otherFeeAmountFsn);
        BigDecimal returnAmountTotal = debitsReturnAmountDchn.add(debitsReturnAmountFsn);
        BigDecimal debitsAmountTotal = debitsAmountDchn.add(debitsAmountFsn);
        BigDecimal paymentAmountTotal = paymentAmountDchn.add(paymentAmountFsn);
        BigDecimal endingBalanceTotal = unpaidAmountFsn.add(unpaidAmountDchn);*/

        EntityValue customerParty = eci.getEntity().find("iam.party.PartyNameView")
                .condition("partyId", this.customerPartyId).one();
        String customerName = customerParty.getString("partyName");
        String customerPseudoId = customerParty.getString("pseudoId");
        Map beginningBalanceOut = eci.getService().sync().name("org.dchn.IamAccountingServices.calculate#CustomerDepositInPeriod")
                                    .parameter("organizationPartyId", rootPartyId)
                                    .parameter("timePeriodId", this.timePeriodIds.get(0))
                                    .parameter("partyId", this.customerPartyId)
                                    .parameter("fromDate", startPeriodTs)
                                    .parameter("thruDate", fromDate)
                                    .parameter("excludedThruDate", "Y")
                                    .call();

        Map endingBalanceOut = eci.getService().sync().name("org.dchn.IamAccountingServices.calculate#CustomerDepositInPeriod")
                                    .parameter("organizationPartyId", rootPartyId)
                                    .parameter("timePeriodId", this.timePeriodIds.get(0))
                                    .parameter("partyId", this.customerPartyId)
                                    .parameter("fromDate", startPeriodTs)
                                    .parameter("thruDate", thruDate)
                                    .call();

        BigDecimal beginningBalanceTotal = (BigDecimal) beginningBalanceOut.get("balanceAmount");
        BigDecimal endingBalanceTotal = (BigDecimal) endingBalanceOut.get("balanceAmount");
        BigDecimal debitsAmountTotal = BigDecimal.ZERO;
        BigDecimal paymentAmountTotal = BigDecimal.ZERO;

        //công nợ tiền hàng
        List<String> invoiceOrderByList = Arrays.asList("invoiceDate", "invoiceId", "invoiceItemSeqId");
        List<String> invoiceStatusIdNotInList = Arrays.asList("InvoiceCancelled", "InvoiceInProcess", "InvoiceIncoming");
        EntityList invoiceItemList = eci.getEntity().find("iam.order.OrderItemBillAndShipment")
                .condition("toPartyId", this.customerPartyId)
                .condition("invoiceStatusId", EntityCondition.NOT_IN, invoiceStatusIdNotInList)
                .condition("invoiceDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("invoiceDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .condition("itemTypeEnumId", EntityCondition.NOT_EQUAL, "ItemMfgBomComponent")
                .orderBy(invoiceOrderByList)
                .list();
        //List invoiceItem công nợ phải thu khác
        EntityList otherInvoiceItemList = eci.getEntity().find("mantle.account.invoice.Invoice")
                .condition("toPartyId", this.customerPartyId)
                .condition("statusId", EntityCondition.NOT_EQUAL, "InvoiceCancelled")
                .condition("invoiceTypeEnumId", EntityCondition.EQUALS, "InvoiceOtherServiceSales")
                .condition("invoiceDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("invoiceDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .condition("invoiceTotal", EntityCondition.NOT_EQUAL, BigDecimal.ZERO)
                .orderBy("invoiceDate")
                .list();

        //list invoiceItem phi van chuyen va phi khac
        EntityValue shippingFeeEnum = eci.getEntity().find("moqui.basic.Enumeration").condition("enumId", "InvoiceShippingFee").one();
        EntityValue otherFeeEnum = eci.getEntity().find("moqui.basic.Enumeration").condition("enumId", "InvoiceOtherFee").one();
        EntityList invoiceItemShippingFeeList = eci.getEntity().find("mantle.work.effort.WorkEffortInvoiceDetail")
                .condition("toPartyId", this.customerPartyId)
                .condition("statusId", EntityCondition.NOT_EQUAL, "InvoiceCancelled")
                .condition("invoiceTypeEnumId", EntityCondition.EQUALS, "InvoiceShippingFee")
                .condition("invoiceDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("invoiceDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .condition("invoiceTotal", EntityCondition.GREATER_THAN, BigDecimal.ZERO)
                .orderBy("invoiceTypeEnumId")
                .list();

        EntityList invoiceItemOtherFeeList = eci.getEntity().find("mantle.work.effort.WorkEffortInvoiceDetail")
                .condition("toPartyId", this.customerPartyId)
                .condition("statusId", EntityCondition.NOT_EQUAL, "InvoiceCancelled")
                .condition("invoiceTypeEnumId", EntityCondition.EQUALS, "InvoiceOtherFee")
                .condition("invoiceDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("invoiceDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .condition("invoiceTotal", EntityCondition.GREATER_THAN, BigDecimal.ZERO)
                .orderBy("invoiceTypeEnumId")
                .list();

        //list invoiceItem hang tra lai
        List<String> returnSelectFields = Arrays.asList("orderId", "pseudoId", "invoiceStatusId", "invoiceDate", "invoiceTotal", "quantity", "amount", "itemTotal", "productName");
        EntityList invoiceItemReturnList = eci.getEntity().find("iam.order.OrderHeaderBillingInvoice")
                .condition("vendorPartyId", this.customerPartyId)
                .condition("customerPartyId", EntityCondition.IN, organizationPartyIds)
                .condition("vendorRoleTypeId", EntityCondition.EQUALS, "Customer")
                .condition("invoiceStatusId", EntityCondition.NOT_EQUAL, "InvoiceCancelled")
                .condition("invoiceDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("invoiceDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .selectFields(returnSelectFields)
                .orderBy(invoiceOrderByList)
                .list();

        //list payment
        List<String> paymentStatus = Arrays.asList("PmntDelivered", "PmntAuthorized");
        EntityList paymentList = eci.getEntity().find("mantle.account.payment.Payment")
                .condition("fromPartyId", this.customerPartyId)
                .condition("statusId", EntityCondition.IN, paymentStatus)
                .condition("paymentTypeEnumId", EntityCondition.EQUALS, "PtInvoicePayment")
                .condition("effectiveDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("effectiveDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .orderBy("effectiveDate")
                .list();

        //hoàn thanh toán cho KH
        EntityList refundPaymentList = eci.getEntity().find("mantle.account.payment.Payment")
                .condition("toPartyId", this.customerPartyId)
                .condition("statusId", EntityCondition.EQUALS, "PmntDelivered")
                .condition("paymentTypeEnumId", EntityCondition.EQUALS, "PtCustomerRefund")
                .condition("effectiveDate", EntityCondition.LESS_THAN_EQUAL_TO, thruDate)
                .condition("effectiveDate", EntityCondition.GREATER_THAN_EQUAL_TO, fromDate)
                .orderBy("effectiveDate")
                .list();

        //List data summary
        List<Map> dataRows = new ArrayList<>();
        BigDecimal quantityTotal = BigDecimal.ZERO;
        String previousWorkEffortId = invoiceItemList != null && invoiceItemList.size() > 0? invoiceItemList.getFirst().getString("workEffortId") : "";
        String previousWeUniversalId = invoiceItemList != null && invoiceItemList.size() > 0? invoiceItemList.getFirst().getString("universalId") : "";
        List<String> workEffortHaveNote = new ArrayList<>();
        //cong no ban hang
        for(EntityValue ii: invoiceItemList){
            Map rowData = new HashedMap();
            String currentWorkEffortId = ii.getString("workEffortId");
            if(!workEffortHaveNote.contains(currentWorkEffortId)){
                EntityValue wef = eci.getEntity().find("mantle.work.effort.WorkEffort").condition("workEffortId", currentWorkEffortId).one();
                if(wef.getString("description") != null){
                    rowData.put("note", wef.get("description"));
                }
                workEffortHaveNote.add(currentWorkEffortId);
            }
            rowData.put("dateTime", ii.get("invoiceDate"));
            rowData.put("universalId", ii.get("universalId"));
            rowData.put("pseudoId", ii.get("pseudoId"));
            rowData.put("productName", ii.get("productName"));
            rowData.put("quantity", ii.get("quantity"));
            rowData.put("amount", ii.get("amount"));
            rowData.put("itemTotal", ii.get("itemTotal"));

            debitsAmountTotal = debitsAmountTotal.add((BigDecimal) ii.get("itemTotal"));
            dataRows.add(rowData);
            if(!previousWorkEffortId.equalsIgnoreCase(currentWorkEffortId)){
                //tim phi cua DO ${previousWorkEffortId}
                BigDecimal feeTotal = this.addWorkEffortFee(dataRows, previousWorkEffortId, previousWeUniversalId, invoiceItemShippingFeeList, invoiceItemOtherFeeList, shippingFeeEnum, otherFeeEnum);
                previousWorkEffortId = currentWorkEffortId;
                previousWeUniversalId = (String)ii.get("universalId");
                debitsAmountTotal = debitsAmountTotal.add(feeTotal);
            }
            quantityTotal = quantityTotal.add((BigDecimal)ii.get("quantity"));
        }
        //tim phi cua DO cuoi cung trong list
        BigDecimal lastFeeTotal = this.addWorkEffortFee(dataRows, previousWorkEffortId, previousWeUniversalId, invoiceItemShippingFeeList, invoiceItemOtherFeeList, shippingFeeEnum, otherFeeEnum);
        debitsAmountTotal = debitsAmountTotal.add(lastFeeTotal);

        //Công nợ phải thu khác của khách hàng
        for(Map otherInv: otherInvoiceItemList){
            Map rowData = new HashedMap();
            rowData.put("dateTime", otherInv.get("invoiceDate"));
            rowData.put("universalId", otherInv.get("invoiceId"));
            rowData.put("quantity", 1);
            rowData.put("amount", otherInv.get("invoiceTotal"));
            rowData.put("itemTotal", otherInv.get("invoiceTotal"));
            rowData.put("note", otherInv.get("description"));
            dataRows.add(rowData);
            debitsAmountTotal = debitsAmountTotal.add((BigDecimal) otherInv.get("invoiceTotal"));
        }

        //hang tra lai
        for(Map returnItem: invoiceItemReturnList){
            Map rowData = new HashedMap();
            BigDecimal returnQuantity = ((BigDecimal)returnItem.get("quantity")).negate();
            rowData.put("dateTime", returnItem.get("invoiceDate"));
            rowData.put("pseudoId", returnItem.get("pseudoId"));
            rowData.put("productName", returnItem.get("productName"));
            rowData.put("quantity", returnQuantity);
            rowData.put("amount", returnItem.get("amount"));
            rowData.put("itemTotal", ((BigDecimal)returnItem.get("itemTotal")).negate());
            dataRows.add(rowData);
            quantityTotal = quantityTotal.add(returnQuantity);
            debitsAmountTotal = debitsAmountTotal.subtract((BigDecimal) returnItem.get("itemTotal"));
        }
        //thanh toan
        for(EntityValue pmnt: paymentList){
            Map rowData = new HashedMap();
            rowData.put("dateTime", pmnt.get("effectiveDate"));
            rowData.put("paymentAmount", pmnt.get("amount"));
            rowData.put("note", pmnt.get("comments"));
            dataRows.add(rowData);
            paymentAmountTotal = paymentAmountTotal.add((BigDecimal)pmnt.get("amount"));
        }

        //hoan thanh toan
        for(EntityValue pmnt: refundPaymentList){
            Map rowData = new HashedMap();
            rowData.put("dateTime", pmnt.get("effectiveDate"));
            rowData.put("paymentAmount", pmnt.get("amount") != null? pmnt.getBigDecimal("amount").negate() : 0);
            rowData.put("note", pmnt.get("comments"));
            dataRows.add(rowData);
            paymentAmountTotal = paymentAmountTotal.subtract((BigDecimal)pmnt.get("amount"));
        }

        //sap xep theo thu tu
        Collections.sort(dataRows, new Comparator<Map>() {
            @Override
            public int compare(Map o1, Map o2) {
                Timestamp effectiveDate1 = (Timestamp)o1.get("dateTime");
                Timestamp effectiveDate2 = (Timestamp)o2.get("dateTime");
                if (effectiveDate1 == null || effectiveDate2 == null){
                    return 0;
                }
                return effectiveDate1.compareTo(effectiveDate2);
            }
        });

        ResourceReference rr = eci.getResource().getLocationReference(location);
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        int rowNum = 1;
        //XSSFCellStyle normalAlignRightStyle = excelUtils.getNormalAlignRightStyle(wb);
        XSSFCellStyle titleHeaderStyle = excelUtils.getTitleHeaderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle boldCenterBorderStyle = excelUtils.getBoldFullBorderStyle(wb);
        XSSFCellStyle boldAlignRightStyle = excelUtils.getBoldAlignRightStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle boldAlignLeftBorderStyle = excelUtils.getBoldAlignLeftBorderStyle(wb);
        //XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle boldAlignCenterBorderStyle = excelUtils.getBoldAlignCenterBorderStyle(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        //XSSFFont normalFont = excelUtils.getNormalFont(wb);
        //XSSFFont boldFont = excelUtils.getBoldFont(wb);

        //Ten KH
        excelUtils.createRowContent(sheet, rowNum, 2, titleHeaderStyle, (short)800, excelUtils.createCellAddresses(rowNum, rowNum, 2, 5), eci.getL10n().localize("CustomerName") + ": " + customerName);
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 2, italicAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 2, 5), eci.getL10n().localize("CustomerPseudoId") + ": " + customerPseudoId);

        rowNum = 5;// start fill item data
        //No dau ky
        excelUtils.createRowContent(sheet, rowNum, 0, boldCenterBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 5), eci.getL10n().localize("LiabilityBeginningBalance"));
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignRightBorderStyle, (short)400, null, beginningBalanceTotal);
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 8, normalBorderStyle, (short)400, null, "");

        rowNum++;
        for(Map rowData: dataRows){
            String universalId = rowData.get("universalId") != null? (String)rowData.get("universalId") : "";
            String pseudoId = rowData.get("pseudoId") != null? (String)rowData.get("pseudoId") : "";
            String productName = rowData.get("productName") != null? (String)rowData.get("productName") : "";

            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, eci.getL10n().format(rowData.get("dateTime"), "dd/MM/yyyy"));
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, universalId);
            excelUtils.createRowContent(sheet, rowNum, 2, normalBorderStyle, (short)400, null, pseudoId);
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, null, productName);
            excelUtils.createRowContent(sheet, rowNum, 4, normalBorderAlignRightStyle, (short)400, null, rowData.get("quantity"));
            excelUtils.createRowContent(sheet, rowNum, 5, normalBorderAlignRightStyle, (short)400, null, rowData.get("amount"));
            excelUtils.createRowContent(sheet, rowNum, 6, normalBorderAlignRightStyle, (short)400, null, rowData.get("itemTotal"));
            excelUtils.createRowContent(sheet, rowNum, 7, normalBorderAlignRightStyle, (short)400, null, rowData.get("paymentAmount"));
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderStyle, (short)400, null, rowData.get("note"));
            rowNum++;
        }
        //tong phat sinh don hang (1)
        /*excelUtils.createRowContent(sheet, rowNum, 0, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignLeftBorderStyle, (short)400, null, eci.getL10n().localize("IamOrderIncurred"));
        excelUtils.createRowContent(sheet, rowNum, 3, boldAlignRightStyle, (short)400, null, eci.getL10n().format(quantityTotal, "#,##0"));
        excelUtils.createRowContent(sheet, rowNum, 4, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 5, boldAlignRightStyle, (short)400, null, eci.getL10n().format(invoiceSaleTotalDchn.add(invoiceSaleTotalFsn), "#,##0"));
        excelUtils.createRowContent(sheet, rowNum, 6, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");*/

        //tong phi (2)
        /*rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignLeftBorderStyle, (short)400, null, eci.getL10n().localize("IamFeeTotal"));
        excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 4, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 5, boldAlignRightBorderStyle, (short)400, null, eci.getL10n().format(feeTotal, "#,##0"));
        excelUtils.createRowContent(sheet, rowNum, 6, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");*/

        //Tong tien tra hang (3)
        /*rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignLeftBorderStyle, (short)400, null, eci.getL10n().localize("IamReturnOrderIncurred"));
        excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 4, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 5, boldAlignRightBorderStyle, (short)400, null, "(" + eci.getL10n().format(returnAmountTotal, "#,##0") + ")");
        excelUtils.createRowContent(sheet, rowNum, 6, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");*/

        //Tong phat sinh = (1) + (2) - (3) va thanh toan
        //rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 2, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 3, boldAlignLeftBorderStyle, (short)400, null, eci.getL10n().localize("IamOrderIncurred"));
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignRightBorderStyle, (short)400, null, quantityTotal);
        excelUtils.createRowContent(sheet, rowNum, 5, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignRightBorderStyle, (short)400, null, debitsAmountTotal);
        excelUtils.createRowContent(sheet, rowNum, 7, boldAlignRightBorderStyle, (short)400, null, paymentAmountTotal);
        excelUtils.createRowContent(sheet, rowNum, 8, normalBorderStyle, (short)400, null, "");

        //No cuoi ky
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 5), eci.getL10n().localize("LiabilityEndingBalance"));
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignRightBorderStyle, (short)400, null, endingBalanceTotal);
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 8, normalBorderStyle, (short)400, null, "");
        return wb;
    }

    private BigDecimal addWorkEffortFee(List dataRows, String workEffortId, String universalId, EntityList invoiceItemShippingFeeList, EntityList invoiceItemOtherFeeList,
                                  EntityValue shippingFeeEnum, EntityValue otherFeeEnum){
        List<Map> shippingFees = invoiceItemShippingFeeList.stream().filter(e -> e.get("workEffortId").equals(workEffortId)) .collect(Collectors.toList());
        List<Map> otherFees = invoiceItemOtherFeeList.stream().filter(e -> e.get("workEffortId").equals(workEffortId)) .collect(Collectors.toList());
        BigDecimal totalFee = BigDecimal.ZERO;
        if(shippingFees.size() > 0){
            BigDecimal shippingFeeTotal = BigDecimal.ZERO;
            for(Map shippingFee: shippingFees){
                shippingFeeTotal = shippingFeeTotal.add((BigDecimal)shippingFee.get("invoiceTotal"));
            }
            Map shippingFeeRow = new HashedMap();
            shippingFeeRow.put("dateTime", shippingFees.get(0).get("invoiceDate"));
            shippingFeeRow.put("universalId", universalId);
            shippingFeeRow.put("pseudoId", shippingFeeEnum.get("description"));
            shippingFeeRow.put("quantity", BigDecimal.ONE);
            shippingFeeRow.put("amount", shippingFeeTotal);
            shippingFeeRow.put("itemTotal", shippingFeeTotal);
            dataRows.add(shippingFeeRow);
            totalFee = totalFee.add(shippingFeeTotal);
        }

        if(otherFees.size() > 0){
            BigDecimal otherFeeTotal = BigDecimal.ZERO;
            for(Map otherFee: otherFees){
                otherFeeTotal = otherFeeTotal.add((BigDecimal)otherFee.get("invoiceTotal"));
            }
            Map otherFeeRow = new HashedMap();
            String note = ((EntityValue)otherFees.get(0)).getString("description");
            otherFeeRow.put("dateTime", otherFees.get(0).get("invoiceDate"));
            otherFeeRow.put("universalId", universalId);
            otherFeeRow.put("pseudoId", otherFeeEnum.get("description"));
            otherFeeRow.put("quantity", BigDecimal.ONE);
            otherFeeRow.put("amount", otherFeeTotal);
            otherFeeRow.put("itemTotal", otherFeeTotal);
            otherFeeRow.put("productName", note);
            dataRows.add(otherFeeRow);
            totalFee = totalFee.add(otherFeeTotal);
        }
        return totalFee;
    }
}
