package org.iam.excel;

import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import javax.swing.text.html.parser.Entity;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CustomerReturnExcel {
    protected ExecutionContextImpl eci;
    private String location;
    private String orderId;

    public CustomerReturnExcel(ExecutionContextImpl eci, String orderId, String location){
        this.eci = eci;
        this.orderId = orderId;
        this.location = location;
    }

    public XSSFWorkbook exportData() throws IOException {
        //int itemDataRowStart = 13;
        List<String> selectFields = Arrays.asList("orderId", "orderPartSeqId", "orderName", "customerName", "customerPseudoId", "vendorPartyName",
                    "vendorPseudoId", "status", "statusId", "grandTotal", "facilityId", "entryDate", "placedDate", "facilityName", "currencyUomId", "agreementId",
                    "customerPartyId", "vendorPartyId");
        EntityList ohList = eci.getEntityFacade().find("iam.order.OrderHeaderPartPtyRole")
                .condition("orderId", orderId)
                .condition("customerRoleTypeId", "OrgInternal")
                .condition("vendorRoleTypeId", "Customer")
                .selectFields(selectFields)
                .list();
        EntityValue oh = ohList.getFirst();
        String customerName = oh.getString("vendorPartyName");
        BigDecimal grandTotal = oh.getBigDecimal("grandTotal");
        BigDecimal quantityTotal = BigDecimal.ZERO;
        List<String> selectItemFields = Arrays.asList("orderId", "orderPartSeqId", "orderItemSeqId", "itemTypeEnumId", "itemTotal", "quantity", "unitAmount", "itemDescription", "productClassEnum", "pseudoId", "productId");
        EntityList itemList = eci.getEntity().find("iam.order.OrderItemInfo")
                .condition("orderId", orderId)
                .condition("itemTypeEnumId", EntityCondition.NOT_EQUAL, "ItemMfgBomComponent")
                .condition("quantityTotal", EntityCondition.NOT_EQUAL, BigDecimal.ZERO)
                .selectFields(selectItemFields)
                .list();
        Map postalAddressOut = eci.getService().sync().name("mantle.party.ContactServices.get#PartyContactInfo")
                .parameter("partyId", oh.get("vendorPartyId"))
                .parameter("postalContactMechPurposeId", "PostalPrimary")
                .call();

        Map telecomNumberOut = eci.getService().sync().name("mantle.party.ContactServices.get#PartyContactInfo")
                .parameter("partyId", oh.get("vendorPartyId"))
                .parameter("telecomContactMechPurposeId", "PhonePrimary")
                .call();
        EntityValue postalAddress = (EntityValue) postalAddressOut.get("postalAddress");
        EntityValue telecomNumber = (EntityValue) telecomNumberOut.get("telecomNumber");
        String customerAddress = (postalAddress != null && postalAddress.get("directions") != null)? (String)postalAddress.get("directions") : "";
        String customerTelecomNumber = (telecomNumber != null && telecomNumber.get("contactNumber") != null)? (String)telecomNumber.get("contactNumber"): "";

        Timestamp placedDate = oh.getTimestamp("placedDate");
        ResourceReference rr = eci.getResource().getLocationReference(location);
        ExcelUtils excelUtils = new ExcelUtils();
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle italicCenterStyle = excelUtils.getItalicStyle(wb);

        //So phieu tra hang
        int rowNum = 5;
        excelUtils.createRowContent(sheet, rowNum, 0, italicAlignCenterStyle, (short)400, null, eci.getL10n().localize("IamNumber") + ": " + orderId);

        //Ten khach hang
        rowNum = 7;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("CustomerName") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(customerName, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("CustomerName") + ": " + customerName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Ngay nhan
        richTextInfos = new ArrayList<>();
        String returnDateFormat = eci.getL10n().format(placedDate, "dd/MM/yyyy");
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamReturnDate") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(returnDateFormat, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamReturnDate") + ": " + returnDateFormat, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //so dien thoai
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamTelNumber") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(customerTelecomNumber, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamTelNumber") + ": " + customerTelecomNumber, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Dia chi
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamAddress") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(customerAddress, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamAddress") + ": " + customerAddress, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        rowNum = 13;//start insert data row
        //item trong phieu bao hanh
        int index = 1;
        for(EntityValue item: itemList){
            //STT
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
            //ten sp
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 1, 5), item.get("itemDescription"));
            //so luong
            excelUtils.createRowContent(sheet, rowNum, 6, normalBorderAlignRightStyle, (short)400, null, item.get("quantity"));
            //don gia
            excelUtils.createRowContent(sheet, rowNum, 7, normalBorderAlignRightStyle, (short)400, null, eci.getL10n().format(item.get("unitAmount"), "#,##0"));
            //thanh tien
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderAlignRightStyle, (short)400, null, eci.getL10n().format(item.get("itemTotal"), "#,##0"));
            rowNum++;
            index++;
            quantityTotal = quantityTotal.add((BigDecimal)item.get("quantity"));
        }//end for
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignRightBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 5), eci.getL10n().localize("IamCommonSum"));
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignRightBorderStyle, (short)400, null, quantityTotal);
        excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 8, boldAlignRightBorderStyle, (short)400, null, eci.getL10n().format(grandTotal, "#,##0"));

        //nguoi van chuyen, nguoi nhan, quan ly kho
        rowNum+=2;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 3), eci.getL10n().localize("IamTransporter"));
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), eci.getL10n().localize("IamAssetReceiver"));
        excelUtils.createRowContent(sheet, rowNum, 7, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), eci.getL10n().localize("IamWarehouseManager"));

        //ky, ho ten
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 3), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 4, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 7, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        return wb;
    }
}
