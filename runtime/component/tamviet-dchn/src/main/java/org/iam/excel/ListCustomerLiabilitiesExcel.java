package org.iam.excel;

import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class ListCustomerLiabilitiesExcel {
    protected ExecutionContextImpl eci;
    private String salePartyId;
    private Integer periodNum;
    private String location;
    private String lazyLoadEvent;
    private List<String> timePeriodIds;
    public ListCustomerLiabilitiesExcel(ExecutionContextImpl eci, String salePartyId, String lazyLoadEvent, Integer periodNum, String location){
        this.eci = eci;
        this.salePartyId = salePartyId;
        this.periodNum = periodNum;
        this.location = location;
        this.lazyLoadEvent = lazyLoadEvent;
        if(periodNum == null){
            this.periodNum = eci.getUser().getNowCalendar().get(Calendar.YEAR);
        }
    }

    public XSSFWorkbook exportData() throws IOException {
        Map orgInternalOut = eci.getService().sync().name("org.dchn.IamPartyServices.get#OrgInternalFirstLevel").call();
        List<String> organizationPartyIds = ((List<Map>)orgInternalOut.get("listData")).stream().map(e -> (String)e.get("fromPartyId")).collect(Collectors.toList());
        ExcelUtils excelUtils = new ExcelUtils();
        EntityList tpList = eci.getEntity().find("mantle.party.time.TimePeriod")
                .condition("periodNum", this.periodNum)
                .condition("timePeriodTypeId", "FiscalYear")
                .condition("partyId", EntityCondition.IN, organizationPartyIds)
                .list();
        this.timePeriodIds = tpList.stream().map(e -> (String)e.get("timePeriodId")).collect(Collectors.toList());
        List conds = new ArrayList();
        conds.add(eci.getEntity().getConditionFactory().makeCondition("glAccountTypeEnumId", EntityCondition.EQUALS, "GatCustomerDeposits"));
        conds.add(eci.getEntity().getConditionFactory().makeCondition("timePeriodId", EntityCondition.IN, this.timePeriodIds));
        EntityValue saleParty = eci.getEntity().find("iam.party.PartyNameView").condition("partyId", salePartyId).one();
        List<Map> itemCustomerLiabilities = null;
        if(saleParty != null){
            EntityList customerRels = eci.getEntity().find("mantle.party.PartyRelationship")
                    .condition("toPartyId", salePartyId)
                    .condition("toRoleTypeId", "SalesRepresentative")
                    .condition("relationshipTypeEnumId", "PrtSaleAgent")
                    .condition("fromRoleTypeId", "Customer")
                    .conditionDate("fromDate", "thruDate", eci.getUser().getNowTimestamp()).list();
            if(customerRels == null || customerRels.size() == 0){
                itemCustomerLiabilities = new ArrayList();
            }else{
                List<String> fromPartyIds = customerRels.stream().map(e -> (String)e.get("fromPartyId")).collect(Collectors.toList());
                conds.add(eci.getEntity().getConditionFactory().makeCondition("toPartyId", EntityCondition.IN, fromPartyIds));
            }
        }
        /*List cusCondOr = new ArrayList();
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("beginningBalanceTotal", EntityCondition.NOT_EQUAL, 0));
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("endingBalanceTotal", EntityCondition.NOT_EQUAL, 0));
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("debitsAmountTotal", EntityCondition.NOT_EQUAL, 0));
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("paymentAmountTotal", EntityCondition.NOT_EQUAL, 0));
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("toPartyDisabled", EntityCondition.NOT_EQUAL, "Y"));
        cusCondOr.add(eci.getEntity().getConditionFactory().makeCondition("toPartyDisabled", EntityCondition.IS_NULL, null));*/
        //EntityCondition havingCondition = eci.getEntity().getConditionFactory().makeCondition(cusCondOr, org.moqui.entity.EntityCondition.OR);
        EntityCondition havingCondition = eci.getEntity().getConditionFactory().makeCondition("endingBalanceTotal", EntityCondition.GREATER_THAN, 0);
        List selectedFields = Arrays.asList("organizationPartyId", "toPartyId", "pseudoId", "partyName", "beginningBalanceDchn", "beginningBalanceFsn", "currencyUomId",
                                            "endingBalanceConvertDchn", "endingBalanceConvertFsn", "debitsAmountDchn", "debitsAmountFsn", "paymentAmountDchn",
                                            "paymentAmountFsn", "beginningBalanceTotal", "endingBalanceTotal", "debitsAmountTotal", "paymentAmountTotal",
                                            "ratioDchn", "ratioFsn", "ratioTotal", "toPartyDisabled");
        if(itemCustomerLiabilities == null){
            Map resultOut = eci.getService().sync().name("org.dchn.IamCommonServices.find#Data")
                    .parameter("entityName", "iam.ledger.account.LiabilityOrgAndPartyToSum")
                    .parameter("selectFields", selectedFields)
                    .parameter("extraCondition", eci.getEntity().getConditionFactory().makeCondition(conds))
                    .parameter("havingCondition", havingCondition)
                    .parameter("lazyLoadEvent", this.lazyLoadEvent)
                    .parameter("defaultOrderField", "-toPartyId")
                    .call();
            itemCustomerLiabilities = (List)resultOut.get("listData");
        }


        ResourceReference rr = eci.getResource().getLocationReference(location);
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle normalAlignRightStyle = excelUtils.getNormalAlignRightStyle(wb);

        int rowNum = 4;
        //Teen nguoi phu trach
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamSaleRepEmployee") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(saleParty != null? saleParty.getString("partyName") : "", normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamSaleRepEmployee") + ": " + (saleParty != null? saleParty.getString("partyName") : ""), richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 1, normalAlignRightStyle, (short)400, null, richTextString);

        rowNum = 10;//start insert data row
        int index = 1;
        BigDecimal totalAmount = BigDecimal.ZERO;
        for(Map item: itemCustomerLiabilities){
            //STT
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
            //Ten khach hàng
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, item.get("partyName"));
            //Nợ đầu kỳ
            excelUtils.createRowContent(sheet, rowNum, 2, normalBorderAlignRightStyle, (short)400, null, item.get("beginningBalanceTotal"));
            //Phát sinh
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderAlignRightStyle, (short)400, null, item.get("debitsAmountTotal"));
            //Thanh toán
            excelUtils.createRowContent(sheet, rowNum, 4, normalBorderAlignRightStyle, (short)400, null, item.get("paymentAmountTotal"));
            //Nợ cuối kỳ
            excelUtils.createRowContent(sheet, rowNum, 5, normalBorderAlignRightStyle, (short)400, null, item.get("endingBalanceTotal"));
            //
            excelUtils.createRowContent(sheet, rowNum, 6, normalBorderStyle, (short)400, null, "");
            excelUtils.createRowContent(sheet, rowNum, 7, normalBorderStyle, (short)400, null, "");
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderStyle, (short)400, null, "");
            excelUtils.createRowContent(sheet, rowNum, 9, normalBorderStyle, (short)400, null, "");
            excelUtils.createRowContent(sheet, rowNum, 10, normalBorderStyle, (short)400, null, "");
            totalAmount = totalAmount.add((BigDecimal)item.get("endingBalanceTotal"));
            rowNum++;
            index++;
        }//end for
        excelUtils.createRowContent(sheet, 5, 3, normalAlignRightStyle, (short)400, null, totalAmount);
        return wb;
    }
}
