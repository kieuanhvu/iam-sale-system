package org.iam.excel;

import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.iam.util.CommonUtils;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WarrantyRequestExcel {
    protected ExecutionContextImpl eci;
    private String requestId;
    private String location;

    public WarrantyRequestExcel(ExecutionContextImpl eci, String requestId, String location){
        this.eci = eci;
        this.requestId = requestId;
        this.location = location;
    }

    public XSSFWorkbook exportData() throws IOException {
        ExcelUtils excelUtils = new ExcelUtils();
        EntityValue warrantyReq = eci.getEntity().find("mantle.request.Request").condition("requestId", requestId).one();
        List<String> selectFields = Arrays.asList("requestId", "requestItemSeqId", "quantity", "quantityResponded", "productId", "productName", "pseudoId", "productClassEnum", "productClassEnumId", "statusId");
        EntityList productItems = eci.getEntity().find("iam.request.RequestItemAndProduct")
                .condition("requestId", requestId)
                .condition("quantity", EntityCondition.GREATER_THAN, BigDecimal.ZERO)
                .selectFields(selectFields)
                .orderBy("requestItemSeqId").list();
        EntityList warrantyDetails = eci.getEntity().find("iam.request.RequestCustomerInfo")
                .condition("requestId", requestId).list();
        EntityValue warrantyDetail = warrantyDetails.getFirst();
        String customerName = warrantyDetail.getString("customerName");
        String address = warrantyDetail.get("directions") != null? warrantyDetail.getString("directions") : "";
        String telNumber = warrantyDetail.get("telContactNumber") != null? warrantyDetail.getString("telContactNumber") : "";
        Timestamp receiveDate = warrantyDetail.getTimestamp("requestDate");
        Timestamp actualCompletionDate = warrantyDetail.getTimestamp("actualCompletionDate");

        ResourceReference rr = eci.getResource().getLocationReference(location);
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle italicCenterStyle = excelUtils.getItalicStyle(wb);

        //So phieu bao hanh
        int rowNum = 5;
        excelUtils.createRowContent(sheet, rowNum, 0, italicAlignCenterStyle, (short)400, null, eci.getL10n().localize("IamNumber") + ": " + (warrantyReq.get("requestName") != null? warrantyReq.get("requestName"): ""));
        //Ten khach hang
        rowNum = 7;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("CustomerName") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(customerName, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("CustomerName") + ": " + customerName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);
        //Ngay nhap
        richTextInfos = new ArrayList<>();
        String receiveDateFormat = eci.getL10n().format(receiveDate, "dd/MM/yyyy");
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamReceivedDate") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(receiveDateFormat, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamReceivedDate") + ": " + receiveDateFormat, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //so dien thoai
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamTelNumber") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(telNumber, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamTelNumber") + ": " + telNumber, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //ngay tra du kien
        richTextInfos = new ArrayList<>();
        String actualCompletionDateFormat = actualCompletionDate != null? eci.getL10n().format(actualCompletionDate, "dd/MM/yyyy") : "";
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamActualReturnDate") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(actualCompletionDateFormat, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamActualReturnDate") + ": " + receiveDateFormat, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //dia chi
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamAddress") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(address, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamAddress") + ": " + address, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        rowNum = 13;//start insert data row
        //item trong phieu bao hanh
        int index = 1;
        for(Map item: productItems){
            //STT
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
            //ma sp
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 1, 2), item.get("pseudoId"));
            //ten sp
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 3, 7), item.get("productName"));
            //so luong
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderAlignRightStyle, (short)400, null, item.get("quantity"));
            rowNum++;
            index++;
        }

        //kiem soat, nguoi xuat hang, khanh hang ky
        rowNum+=3;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 3), eci.getL10n().localize("IamQualityControlParty"));
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 5), eci.getL10n().localize("IamWarehouseManager"));
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 6, 8), eci.getL10n().localize("IamCustomerSignature"));
        //ky, ho ten
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 3), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 4, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 5), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 6, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 6, 8), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        return wb;
    }
}
