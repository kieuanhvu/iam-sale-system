package org.iam.common;

public class Constant {
    public static final String STARTS_WITH = "startsWith";
    public static final String CONTAINS = "contains";
    public static final String EQUALS = "equals";
    public static final String NOT_EQUALS = "notEquals";
    public static final String IN = "in";
    public static final String LESS_THAN = "lt";
    public static final String LESS_THAN_EQUAL_TO = "lte";
    public static final String GREATER_THAN = "gt";
    public static final String GREATER_THAN_EQUAL_TO = "gte";
    public static final String NUMERIC = "numeric";
    public static final String BY_DATE = "byDate";
    public static final String ENCODE_ENC = "UTF-8";
}
