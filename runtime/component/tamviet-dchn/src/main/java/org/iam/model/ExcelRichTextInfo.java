package org.iam.model;

import org.apache.poi.xssf.usermodel.XSSFFont;

public class ExcelRichTextInfo {
    private String text;
    private XSSFFont font;

    public ExcelRichTextInfo(String text, XSSFFont font) {
        this.text = text;
        this.font = font;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFont(XSSFFont font) {
        this.font = font;
    }

    public XSSFFont getFont() {
        return font;
    }
}
