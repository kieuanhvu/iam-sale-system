package org.iam.util;

import groovy.json.JsonSlurper;
import org.iam.common.Constant;
import org.moqui.context.ExecutionContext;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityConditionFactory;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.impl.entity.EntityJavaUtil;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SearchUtils {
    public static Map<String, EntityCondition> buildConditionFromString(ExecutionContext ec, Map filter, List<String> havingCondFields) {
        return buildConditionFromString(ec, filter, havingCondFields, null);
    }

    public static Map<String, EntityCondition> buildConditionFromString(ExecutionContext ec, Map filter, List<String> havingCondFields, List<String> excludeFields) {
        if(filter == null){
            return null;
        }
        try{
            Map<String, EntityCondition> mapCondition = new HashMap<>();
            JsonSlurper jsonSlurper = new JsonSlurper();
            List<EntityCondition> whereConds = new ArrayList<EntityCondition>();
            List<EntityCondition> havingConds = new ArrayList<EntityCondition>();
            Map<String, Object> jsonMap = filter;
            for (Map.Entry<String, Object> entry : jsonMap.entrySet()) {
                String fieldName = entry.getKey();
                if(excludeFields != null && excludeFields.contains(fieldName)){
                    continue;
                }
                Object searchCondObj = entry.getValue();
                if (searchCondObj instanceof Map) {
                    Map<String, Object> searchCondMap = (Map<String, Object>) searchCondObj;
                    Object searchValue = searchCondMap.get("value");
                    String matchMode = (String)searchCondMap.get("matchMode");
                    EntityCondition condition = buildCondition(ec, ec.getEntity().getConditionFactory(), fieldName, matchMode, searchValue);
                    if (condition != null) {
                        if(havingCondFields == null || !havingCondFields.contains(fieldName)){
                            whereConds.add(condition);
                        }else {
                            havingConds.add(condition);
                        }
                    }
                }// end if
            }//end for

            if (whereConds.size() > 0) {
                mapCondition.put("whereConds", ec.getEntity().getConditionFactory().makeCondition(whereConds));
            }
            if (havingConds.size() > 0) {
                mapCondition.put("havingConds", ec.getEntity().getConditionFactory().makeCondition(havingConds));
            }
            return mapCondition;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String buildConditionFromString(ExecutionContext ec, Map filter, boolean isCamelCaseConvert) {
        if(filter == null){
            return null;
        }
        String sqlWhere = null;
        try{
            JsonSlurper jsonSlurper = new JsonSlurper();
            List<String> conds = new ArrayList<String>();
            Map<String, Object> jsonMap = filter;
            for (Map.Entry<String, Object> entry : jsonMap.entrySet()) {
                String fieldName = entry.getKey();
                if(isCamelCaseConvert){
                    fieldName = EntityJavaUtil.camelCaseToUnderscored(fieldName);
                }
                Object searchCondObj = entry.getValue();
                if (searchCondObj instanceof Map) {
                    Map<String, Object> searchCondMap = (Map<String, Object>) searchCondObj;
                    Object searchValue = searchCondMap.get("value");
                    String matchMode = (String)searchCondMap.get("matchMode");
                    String condition = convertToSqlWhere(ec, fieldName, matchMode, searchValue);
                    if (condition != null) {
                        conds.add(condition);
                    }
                }// end if
            }//end for
            if(conds.size() > 0){
                StringBuilder sqlWhereBuilder = new StringBuilder();
                for(int i = 0; i < conds.size(); i++){
                    sqlWhereBuilder.append(" and " + conds.get(i));
                }
                sqlWhere = sqlWhereBuilder.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return sqlWhere;
    }

    public static String buildOrderSql(String alwaysOrderByField, String sortField, String sortOrder, String multiSortMeta, boolean isCamelCaseConvert){
        StringBuilder orderBuilder = new StringBuilder("  order by ");
        boolean hasAlwaysOrderByField = false;
        if(alwaysOrderByField != null && alwaysOrderByField.length() > 0){
            orderBuilder.append(alwaysOrderByField);
            hasAlwaysOrderByField = true;
        }
        if(multiSortMeta != null && multiSortMeta.trim().length() > 0){
            JsonSlurper jsonSlurper = new JsonSlurper();
            Object jsonObject = jsonSlurper.parseText(multiSortMeta);
            if (jsonObject instanceof List) {
                List<Map<String, Object>> listSort = (List<Map<String, Object>>) jsonObject;
                if(listSort.size() > 0 && hasAlwaysOrderByField){
                    orderBuilder.append(", ");
                }
                for(int i = 0; i < listSort.size(); i++){
                    Map<String, Object> fieldMap = listSort.get(i);
                    String fieldName = (String)fieldMap.get("field");
                    if(isCamelCaseConvert){
                        fieldName = EntityJavaUtil.camelCaseToUnderscored(fieldName);
                    }
                    Integer sort = (Integer)fieldMap.get("order");
                    if(sort < 0){
                        fieldName = fieldName + " desc ";
                    }
                    if(i > 0){
                        orderBuilder.append(" , ");
                    }
                    orderBuilder.append(fieldName);
                }
                return orderBuilder.toString();
            }
        }else if(sortField != null && sortField.trim().length() > 0){
            int sort = 1;
            try{
                sort = Integer.parseInt(sortOrder);
            }catch (NumberFormatException e){}
            if(isCamelCaseConvert){
                sortField = EntityJavaUtil.camelCaseToUnderscored(sortField);
            }
            if(sort < 0){
                sortField = sortField + " desc ";
            }
            if(hasAlwaysOrderByField){
                orderBuilder.append(", ");
            }
            orderBuilder.append(sortField);
            return orderBuilder.toString();
        }
        return null;
    }

    public static List<String> buildOrderByFieldNames(String sortField, String sortOrder, String multiSortMeta){
        List<String> fieldNames = new ArrayList<>();
        if(multiSortMeta != null && multiSortMeta.trim().length() > 0){
            JsonSlurper jsonSlurper = new JsonSlurper();
            Object jsonObject = jsonSlurper.parseText(multiSortMeta);
            if (jsonObject instanceof List) {
                List<Map<String, Object>> listSort = (List<Map<String, Object>>) jsonObject;
                for(Map<String, Object> fieldMap: listSort){
                    String fieldName = (String)fieldMap.get("field");
                    Integer sort = (Integer)fieldMap.get("order");
                    if(sort < 0){
                        fieldName = "-" + fieldName;
                    }
                    fieldNames.add(fieldName);
                }
            }
        }else if(sortField != null && sortField.trim().length() > 0){
            int sort = 1;
            try{
                sort = Integer.parseInt(sortOrder);
            }catch (NumberFormatException e){}

            if(sort < 0){
                sortField = "-" + sortField;
            }
            fieldNames.add(sortField);
        }
        return fieldNames;
    }

    private static String convertToSqlWhere(ExecutionContext ec, String fieldName, String matchMode, Object searchValue){
        if (searchValue == null) {
            return null;
        }

        if (matchMode == null || matchMode.trim().equals("")) {
            matchMode = Constant.EQUALS;
        }
        String sqlWhere = null;
        switch (matchMode){
            case Constant.EQUALS:
                sqlWhere = fieldName + " = '" + searchValue + "' ";
                break;
            case Constant.IN:
                List<String> searchValueList = null;
                if(searchValue instanceof List){
                    searchValueList = (List<String>)searchValue;
                }else if(searchValue instanceof String){
                    searchValueList = Arrays.stream(((String) searchValue).split(",")).map(str -> str.trim()).collect(Collectors.toList());
                }
                if(searchValueList != null && searchValueList.size() > 0){
                    StringBuilder inWhereSql = new StringBuilder("(");
                    for(int i = 0; i < searchValueList.size(); i++){
                        if(i > 0){
                            inWhereSql.append(",");
                        }
                        inWhereSql.append("'" + searchValueList.get(i) + "'");
                    }
                    inWhereSql.append(")");
                    sqlWhere = fieldName + " in " + inWhereSql.toString();
                }
                break;
            case Constant.CONTAINS:
                String tempSearch = searchValue != null? searchValue.toString().toUpperCase().trim(): "";
                sqlWhere = "upper(" + fieldName + ") LIKE '%" +  tempSearch + "%'";
                break;
            case Constant.GREATER_THAN:
                sqlWhere = fieldName + " > " + searchValue;
                break;
            case Constant.GREATER_THAN_EQUAL_TO:
                sqlWhere = fieldName + " >= " + searchValue;
                break;
            case Constant.LESS_THAN:
                sqlWhere = fieldName + " < " + searchValue;
                break;
            case Constant.LESS_THAN_EQUAL_TO:
                sqlWhere = fieldName + " <= " + searchValue;
                break;
            case Constant.NOT_EQUALS:
                sqlWhere = fieldName + ((searchValue instanceof String)? (" <> '" + searchValue + "' ") : searchValue);
                break;
            case Constant.STARTS_WITH:
                sqlWhere = fieldName + " LIKE '" + searchValue + "%'";
                break;
            case Constant.NUMERIC:
                String searchValueStr = searchValue.toString();
                if(searchValueStr.contains(">=")){
                    String actualValue = searchValueStr.replaceAll(">=", "").trim();
                    sqlWhere = fieldName + " >= " + actualValue;
                }else if(searchValueStr.contains("<=")){
                    String actualValue = searchValueStr.replaceAll("<=", "").trim();
                    sqlWhere = fieldName + " <= " + actualValue;
                }else if(searchValueStr.contains(">")){
                    String actualValue = searchValueStr.replaceAll(">", "").trim();
                    sqlWhere = fieldName + " > " + actualValue;
                }else if(searchValueStr.contains("<")){
                    String actualValue = searchValueStr.replaceAll("<", "").trim();
                    sqlWhere = fieldName + " < " + actualValue;
                }else{
                    sqlWhere = fieldName + " = " + searchValue;
                }
            case Constant.BY_DATE:
                String dateStr = searchValue.toString();
                Timestamp dateSearch = new Timestamp(Long.parseLong(dateStr));
                Timestamp startDate = CommonUtils.getStartOfDate((ExecutionContextImpl) ec, dateSearch);
                Timestamp endDate = CommonUtils.getEndOfDate((ExecutionContextImpl) ec, dateSearch);
                String startDateFormat = ec.getL10n().format(startDate, "yyyy-MM-dd HH:mm:ss");
                String endDateFormat = ec.getL10n().format(endDate, "yyyy-MM-dd HH:mm:ss");
                sqlWhere = fieldName + " >= '" + startDateFormat + "' and " + fieldName + " <= '" + endDateFormat + "' ";
            break;
        }
        return sqlWhere;
    }

    private static EntityCondition buildCondition(ExecutionContext ec, EntityConditionFactory ecf, String fieldName, String matchMode, Object searchValue) {
        if (searchValue == null) {
            return null;
        }

        if (matchMode == null || matchMode.trim().equals("")) {
            matchMode = Constant.EQUALS;
        }
        if(searchValue instanceof String){
            searchValue = ((String) searchValue).trim();
        }
        EntityCondition cond = null;
        switch (matchMode){
            case Constant.EQUALS:
                cond = ecf.makeCondition(fieldName, EntityCondition.EQUALS, searchValue);
                break;
            case Constant.IN:
                List searchValueList = null;
                if(searchValue instanceof List){
                    searchValueList = (List)searchValue;
                }else if(searchValue instanceof String){
                    searchValueList = Arrays.stream(((String) searchValue).split(",")).map(str -> str.trim()).collect(Collectors.toList());
                }
                cond = ecf.makeCondition(fieldName, EntityCondition.IN, searchValueList);
                break;
            case Constant.CONTAINS:
                cond = ecf.makeCondition(fieldName, EntityCondition.LIKE, "%" + searchValue + "%").ignoreCase();
                break;
            case Constant.GREATER_THAN:
                cond = ecf.makeCondition(fieldName, EntityCondition.GREATER_THAN, searchValue);
                break;
            case Constant.GREATER_THAN_EQUAL_TO:
                cond = ecf.makeCondition(fieldName, EntityCondition.GREATER_THAN_EQUAL_TO, searchValue);
                break;
            case Constant.LESS_THAN:
                cond = ecf.makeCondition(fieldName, EntityCondition.LESS_THAN, searchValue);
                break;
            case Constant.LESS_THAN_EQUAL_TO:
                cond = ecf.makeCondition(fieldName, EntityCondition.LESS_THAN_EQUAL_TO, searchValue);
                break;
            case Constant.NOT_EQUALS:
                cond = ecf.makeCondition(fieldName, EntityCondition.NOT_EQUAL, searchValue);
                break;
            case Constant.STARTS_WITH:
                cond = ecf.makeCondition(fieldName, EntityCondition.LIKE, searchValue + "%");
                break;
            case Constant.NUMERIC:
                String searchValueStr = searchValue.toString();
                if(searchValueStr.contains(">=")){
                    BigDecimal actualValue = new BigDecimal(searchValueStr.replaceAll(">=", "").trim());
                    cond = ecf.makeCondition(fieldName, EntityCondition.GREATER_THAN_EQUAL_TO, actualValue);
                }else if(searchValueStr.contains("<=")){
                    BigDecimal actualValue = new BigDecimal(searchValueStr.replaceAll("<=", "").trim());
                    cond = ecf.makeCondition(fieldName, EntityCondition.LESS_THAN_EQUAL_TO, actualValue);
                }else if(searchValueStr.contains(">")){
                    BigDecimal actualValue = new BigDecimal(searchValueStr.replaceAll(">", "").trim());
                    cond = ecf.makeCondition(fieldName, EntityCondition.GREATER_THAN, actualValue);
                }else if(searchValueStr.contains("<")){
                    BigDecimal actualValue = new BigDecimal(searchValueStr.replaceAll("<", "").trim());
                    cond = ecf.makeCondition(fieldName, EntityCondition.LESS_THAN, actualValue);
                }else{
                    BigDecimal actualValue = new BigDecimal(searchValueStr.trim());
                    cond = ecf.makeCondition(fieldName, EntityCondition.EQUALS, actualValue);
                }
                break;
            case Constant.BY_DATE:
                String dateStr = searchValue.toString();
                Timestamp dateSearch = new Timestamp(Long.parseLong(dateStr));
                Timestamp startDate = CommonUtils.getStartOfDate((ExecutionContextImpl) ec, dateSearch);
                Timestamp endDate = CommonUtils.getEndOfDate((ExecutionContextImpl) ec, dateSearch);
                List dateConds = new ArrayList();
                dateConds.add(ecf.makeCondition(fieldName, EntityCondition.LESS_THAN_EQUAL_TO, endDate));
                dateConds.add(ecf.makeCondition(fieldName, EntityCondition.GREATER_THAN_EQUAL_TO, startDate));
                cond = ecf.makeCondition(dateConds);
            break;
        }
        return cond;
    }
}
