package org.iam.excel;

import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.iam.util.CommonUtils;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;

public class SupplierLiabilityExcel {
    protected ExecutionContextImpl eci;
    private String supplierId;
    private String timePeriodId;
    private String organizationPartyId;
    private String location;

    public SupplierLiabilityExcel(ExecutionContextImpl eci, String supplierId, String timePeriodId, String organizationPartyId, String location){
        this.eci = eci;
        this.supplierId = supplierId;
        this.timePeriodId = timePeriodId;
        this.organizationPartyId = organizationPartyId;
        this.location = location;
        if(this.timePeriodId == null){
            String currYear = eci.getL10n().format(eci.getUser().getNowCalendar().get(Calendar.YEAR), "0000");
            Date fromDate = eci.getL10n().parseDate(currYear + "-01-01", "yyyy-MM-dd");
            Map timePeriodOut = eci.getService().sync().name("mantle.party.TimeServices.getOrCreate#TimePeriod")
                    .parameter("partyId", this.organizationPartyId)
                    .parameter("timePeriodTypeId", "FiscalYear")
                    .parameter("fromDate", fromDate).call();
            this.timePeriodId = (String)timePeriodOut.get("timePeriodId");
        }
    }

    public XSSFWorkbook exportData() throws IOException {
        Locale locale = CommonUtils.getGlobalLocale();
        TimeZone timeZone = CommonUtils.getGlobalTimeZone();
        ExcelUtils excelUtils = new ExcelUtils();
        Map periodInfoOut = eci.getService().sync().name("mantle.party.TimeServices.get#TimePeriodInfo")
                .parameter("timePeriodId", this.timePeriodId).call();
        Timestamp fromDate = (Timestamp)periodInfoOut.get("fromTimestamp");
        Timestamp thruDate = (Timestamp)periodInfoOut.get("thruTimestamp");
        Timestamp nowTimestamp = eci.getUser().getNowTimestamp();
        if(nowTimestamp.before(thruDate)){
            thruDate = nowTimestamp;
        }
        ResourceReference rr = eci.getResource().getLocationReference(location);

        /*String fromDateStr = eci.getL10n().format(fromDate, "dd-MM-yyyy");
        String thruDateStr = eci.getL10n().format(thruDate, "dd-MM-yyyy");
        int lastCol = 14, firstCol = 0;*/
        Map liabilityOverviewOut = eci.getService().sync().name("org.dchn.IamAccountPayableServices.get#LiabilitySupplierOverview")
                .parameter("supplierId", supplierId)
                .parameter("timePeriodId", timePeriodId)
                .parameter("includePmntNotAuthorized", true)
                .parameter("organizationPartyId", organizationPartyId).call();
        String supplierName = (String)liabilityOverviewOut.get("supplierName");
        String supplierPseudoId = (String)liabilityOverviewOut.get("supplierPseudoId");
        String isForeign = (String)liabilityOverviewOut.get("isForeign");
        BigDecimal beginningBalance = (BigDecimal)liabilityOverviewOut.get("beginningBalance");
        BigDecimal paymentAmount = (BigDecimal)liabilityOverviewOut.get("paymentAmount");
        BigDecimal debitsAmount = (BigDecimal)liabilityOverviewOut.get("debitsAmount");
        BigDecimal unpaidAmount = (BigDecimal)liabilityOverviewOut.get("unpaidAmount");
        String baseCurrencyUomId = (String)liabilityOverviewOut.get("baseCurrencyUomId");
        //Danh sách invoice
        Map invoiceOut = eci.getService().sync().name("org.dchn.IamAccountPayableServices.get#SupplierInvoices")
                .parameter("timePeriodId",timePeriodId)
                .parameter("supplierId", supplierId)
                .parameter("organizationPartyId",organizationPartyId).call();
        List<Map> invoiceList = (List)invoiceOut.get("listData");

        Map paymentOut = eci.getService().sync().name("org.dchn.IamAccountPayableServices.get#SupplierPayments")
                .parameter("timePeriodId",timePeriodId)
                .parameter("supplierId", supplierId)
                .parameter("organizationPartyId",organizationPartyId).call();
        List<Map> paymentList = (List)paymentOut.get("listData");

        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);

        //XSSFCellStyle titleHeaderStyle = excelUtils.getTitleHeaderStyle(wb);
        //XSSFCellStyle normalStyle = excelUtils.getNormalStyle(wb);
        XSSFCellStyle normalAlignRightStyle = excelUtils.getNormalAlignRightStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle boldAlignRightStyle = excelUtils.getBoldAlignRightStyle(wb);
        //XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        //XSSFCellStyle boldAlignCenterBorderStyle = excelUtils.getBoldAlignCenterBorderStyle(wb);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        //Tên NCC
        int rowNum = 2;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("SupplierNameMultiLanguage") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(supplierName, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("SupplierNameMultiLanguage") + ": " + supplierName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);
        //No dau ky
        excelUtils.createRowContent(sheet, rowNum, 4, normalAlignRightStyle, (short)400, null, eci.getL10n().format(beginningBalance, "#,##0.00", locale, timeZone) + " " + baseCurrencyUomId);

        //Ma NCC
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("SupplierPseudoIdMultiLanguage") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(supplierPseudoId, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("SupplierPseudoIdMultiLanguage") + ": " + supplierPseudoId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);
        //phat sinh trong ky
        excelUtils.createRowContent(sheet, rowNum, 4, normalAlignRightStyle, (short)400, null, eci.getL10n().format(debitsAmount, "#,##0.00", locale, timeZone) + " " + baseCurrencyUomId);

        //Loai tien te
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamCurrencyMultiLanguage") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(baseCurrencyUomId, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamCurrencyMultiLanguage") + ": " + baseCurrencyUomId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);
        //Thanh toan trong ky
        excelUtils.createRowContent(sheet, rowNum, 4, normalAlignRightStyle, (short)400, null, eci.getL10n().format(paymentAmount, "#,##0.00", locale, timeZone) + " " + baseCurrencyUomId);

        //No cuoi ky
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 4, normalAlignRightStyle, (short)400, null, eci.getL10n().format(unpaidAmount, "#,##0.00", locale, timeZone) + " " + baseCurrencyUomId);

        List<Map> dataRows = new ArrayList<>();
        BigDecimal invoiceTotalSum = BigDecimal.ZERO;
        BigDecimal paymentTotalSum = BigDecimal.ZERO;
        for(Map invoice: invoiceList){
            String currencyUomId = (String)invoice.get("currencyUomId");
            Map rowData = new HashedMap();
            rowData.put("effectiveDate", invoice.get("invoiceDate"));
            rowData.put("comments", invoice.get("displayId"));
            rowData.put("invoiceTotal", invoice.get("invoiceTotal"));
            invoiceTotalSum = invoiceTotalSum.add(invoice.get("invoiceTotal") != null? (BigDecimal)invoice.get("invoiceTotal"): BigDecimal.ZERO);
            dataRows.add(rowData);
        }

        for(Map payment: paymentList){
            Map rowData = new HashedMap();
            String amountUomId = (String)payment.get("amountUomId");
            BigDecimal amount = payment.get("amount") != null? (BigDecimal) payment.get("amount") : BigDecimal.ZERO;
            rowData.put("effectiveDate", payment.get("effectiveDate"));
            rowData.put("comments", payment.get("comments"));
            rowData.put("paymentAmount", eci.getL10n().format(amount, "#,##0.00", locale, timeZone) + " " + amountUomId);
            if(!baseCurrencyUomId.equalsIgnoreCase(amountUomId)){
                rowData.put("localConversionRate", eci.getL10n().format(payment.get("localConversionRate"), "#,##0.0000", locale, timeZone));
                amount = payment.get("localCurrencyAmount") != null? (BigDecimal) payment.get("localCurrencyAmount") : BigDecimal.ZERO;
                rowData.put("localCurrencyAmount", amount);
            }else{
                rowData.put("localCurrencyAmount", amount);
            }
            paymentTotalSum = paymentTotalSum.add(amount);
            dataRows.add(rowData);
        }
        //sap xep thu tu theo ngay
        Collections.sort(dataRows, new Comparator<Map>() {
            @Override
            public int compare(Map o1, Map o2) {
                Timestamp effectiveDate1 = (Timestamp)o1.get("effectiveDate");
                Timestamp effectiveDate2 = (Timestamp)o2.get("effectiveDate");
                if (effectiveDate1 == null || effectiveDate2 == null){
                    return 0;
                }
                return effectiveDate1.compareTo(effectiveDate2);
            }
        });
        rowNum = 9;
        for(Map rowData: dataRows){
            String comments = rowData.get("comments") != null? (String)rowData.get("comments") : "";
            String temPaymentAmount = rowData.get("paymentAmount") != null? (String)rowData.get("paymentAmount") : "";
            String localConversionRate = rowData.get("localConversionRate") != null? (String)rowData.get("localConversionRate") : "";
            BigDecimal localCurrencyAmount = (BigDecimal)rowData.get("localCurrencyAmount");
            BigDecimal invoiceTotal = (BigDecimal)rowData.get("invoiceTotal");
            localCurrencyAmount = localCurrencyAmount != null? localCurrencyAmount.setScale(2, RoundingMode.HALF_UP) : null;
            invoiceTotal = invoiceTotal != null? invoiceTotal.setScale(2, RoundingMode.HALF_UP) : null;
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, eci.getL10n().format(rowData.get("effectiveDate"), "dd/MM/yyyy"));
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, comments);
            excelUtils.createRowContent(sheet, rowNum, 2, normalBorderAlignRightStyle, (short)400, null, temPaymentAmount);
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderAlignRightStyle, (short)400, null, localConversionRate);
            excelUtils.createRowContent(sheet, rowNum, 4, normalBorderAlignRightStyle, CellType.NUMERIC, (short)400, null, localCurrencyAmount);
            excelUtils.createRowContent(sheet, rowNum, 5, normalBorderAlignRightStyle, CellType.NUMERIC, (short)400, null, invoiceTotal);
            rowNum++;
        }
        //Tong tien
        paymentTotalSum = paymentTotalSum.setScale(2, RoundingMode.HALF_UP);
        invoiceTotalSum = invoiceTotalSum.setScale(2, RoundingMode.HALF_UP);
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignRightStyle, (short)400, null, paymentTotalSum);
        excelUtils.createRowContent(sheet, rowNum, 5, boldAlignRightStyle, (short)400, null, invoiceTotalSum);
        return wb;
    }

    private void setColumnWidth(XSSFSheet sheet) {
        sheet.setColumnWidth(0, 1200);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 3200);
        sheet.setColumnWidth(4, 3000);
        sheet.setColumnWidth(5, 2500);
        sheet.setColumnWidth(6, 3500);
        sheet.setColumnWidth(7, 3500);
        sheet.setColumnWidth(8, 3900);
        sheet.setColumnWidth(9, 3200);
        sheet.setColumnWidth(10, 3700);
        sheet.setColumnWidth(11, 3200);
        sheet.setColumnWidth(12, 3300);
        sheet.setColumnWidth(13, 3900);
        sheet.setColumnWidth(14, 3900);
    }
}
