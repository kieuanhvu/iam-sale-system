package org.iam.excel;

import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WorkEffortFinReceiveExcel {
    protected ExecutionContextImpl eci;
    private String workEffortId;
    private String location;

    public WorkEffortFinReceiveExcel(ExecutionContextImpl eci, String workEffortId, String location){
        this.eci = eci;
        this.workEffortId = workEffortId;
        this.location = location;
    }

    public XSSFWorkbook exportData() throws IOException {
        ExcelUtils excelUtils = new ExcelUtils();
        EntityList wefList = eci.getEntity().find("iam.work.effort.WorkEffortProductionRunSum")
                .condition("workEffortId", workEffortId).list();
        EntityValue wef = wefList.getFirst();
        String universalId = wef.get("universalId") != null? wef.getString("universalId") : "";
        Timestamp actualStartDate = wef.getTimestamp("actualStartDate");
        String organizationPseudoId = wef.get("organizationPseudoId") != null? wef.getString("organizationPseudoId") : "";
        String description = wef.get("description") != null? wef.getString("description") : "";
        String status = wef.getString("status");
        List<String> selectFields = Arrays.asList("workEffortId", "productId", "fromDate", "pseudoId", "productName", "estimatedQuantity");
        EntityList productItems = eci.getEntity().find("mantle.work.effort.WorkEffortProductDetail")
                .condition("workEffortId", workEffortId)
                .condition("typeEnumId", "WeptProduce")
                .selectFields(selectFields)
                .list();

        ResourceReference rr = eci.getResource().getLocationReference(location);
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle italicCenterStyle = excelUtils.getItalicStyle(wb);

        //Mã phiếu nhập
        int rowNum = 6;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamAssetReceiptCode") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(universalId, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamAssetReceiptCode") + ": " + universalId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Tên phiếu
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamAssetReceiptName") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(description, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamAssetReceiptName") + ": " + description, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //Ngay nhap
        rowNum++;
        richTextInfos = new ArrayList<>();
        String receiveDateFormat = eci.getL10n().format(actualStartDate, "dd/MM/yyyy");
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamReceivedDate") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(receiveDateFormat, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamReceivedDate") + ": " + receiveDateFormat, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Trang thai
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("Status") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(status, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("Status") + ": " + status, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //Tổ chức
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamOrganization") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(organizationPseudoId, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamOrganization") + ": " + organizationPseudoId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        rowNum = 12;//start insert data row
        int index = 1;
        BigDecimal quantityTotal = BigDecimal.ZERO;
        for(Map item: productItems){
            //STT
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
            //ma sp
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 1, 2), item.get("pseudoId"));
            //ten sp
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 3, 7), item.get("productName"));
            //so luong
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderAlignRightStyle, (short)400, null, item.get("estimatedQuantity"));
            quantityTotal = quantityTotal.add((BigDecimal)item.get("estimatedQuantity"));
            rowNum++;
            index++;
        }
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignRightBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 7), eci.getL10n().localize("IamCommonSum"));
        excelUtils.createRowContent(sheet, rowNum, 8, boldAlignRightBorderStyle, (short)400, null, quantityTotal);

        //Quan ly san xuat, Kho hien truong, To truong san xuat, quan ly kho
        rowNum+=3;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 2), eci.getL10n().localize("IamProductionManager"));
        excelUtils.createRowContent(sheet, rowNum, 3, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 3, 4), eci.getL10n().localize("IamCurrentWarehouse"));
        excelUtils.createRowContent(sheet, rowNum, 5, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 5, 6), eci.getL10n().localize("IamProductionLeader"));
        excelUtils.createRowContent(sheet, rowNum, 7, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), eci.getL10n().localize("IamWarehouseManager"));

        //ky, ho ten
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 0, 2), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 3, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 3, 4), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 5, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 5, 6), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 7, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        return wb;
    }
}
