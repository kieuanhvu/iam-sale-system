package org.iam.excel;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.iam.util.CommonUtils;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class PurchaseOrderExcel {
    protected ExecutionContextImpl eci;
    private String location;
    private String orderId;
    private String geoId;
    public PurchaseOrderExcel(ExecutionContextImpl eci, String location, String orderId, String geoId){
        this.eci = eci;
        this.location = location;
        this.orderId = orderId;
        this.geoId = geoId;
    }

    public XSSFWorkbook exportData() throws IOException {
        //Locale locale = CommonUtils.getGlobalLocale();
        //TimeZone timeZone = CommonUtils.getGlobalTimeZone();
        ExcelUtils excelUtils = new ExcelUtils();
        ResourceReference rr = eci.getResource().getLocationReference(location);
        EntityList orderPartList = eci.getEntity().find("mantle.order.OrderPart")
                                            .condition("orderId", this.orderId).list();
        EntityValue orderPart = orderPartList.getFirst();
        EntityValue orderHeader = eci.getEntity().find("mantle.order.OrderHeader")
                .condition("orderId", orderId).one();
        EntityValue vendorParty = eci.getEntity().find("iam.party.PartyNameView")
                .condition("partyId", orderPart.get("vendorPartyId")).one();
        /*EntityValue vendorPartyAcctg = eci.getEntity().find("mantle.ledger.config.PartyAcctgPreference")
                .condition("partyId", orderPart.get("vendorPartyId")).one();*/
        String supplierName = vendorParty.getString("partyName");
        String supplierPseudoId = vendorParty.getString("pseudoId");
        String currencyUomId = orderHeader.getString("currencyUomId");
        String orderDisplayId = orderHeader.getString("displayId");
        String orderNote = orderHeader.getString("orderName");
        Map itemProductOut = eci.getService().sync().name("org.dchn.IamPurchaseOrderServices.get#OrderItems")
                .parameter("orderId", orderId).call();
        Map itemExpenseOut = eci.getService().sync().name("org.dchn.IamPurchaseOrderServices.get#OrderItemExpenses")
                .parameter("orderId", orderId).call();
        List<Map> itemProductList = (List<Map>)itemProductOut.get("listData");
        List<Map> itemExpenseList = (List<Map>)itemExpenseOut.get("listData");
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFCellStyle normalAlignRightStyle = excelUtils.getNormalAlignRightStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle boldAlignLeftBorderStyle = excelUtils.getBoldAlignLeftBorderStyle(wb);
        XSSFCellStyle boldAlignRightStyle = excelUtils.getBoldAlignRightStyle(wb);
        XSSFCellStyle boldAlignLeftStyle = excelUtils.getBoldStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle italicCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle italicAlignLeftStyle = excelUtils.getItalicLeftStyle(wb);
        XSSFCellStyle italicAlignRightStyle = excelUtils.getItalicRightStyle(wb);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        int rowNum = 5;
        int lastCol = 9, firstCol = 0;
        String orderNumberStr = eci.getL10n().localize("IamNumberMultiLanguage") + ": " + orderDisplayId;
        //So don hang
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, null, orderNumberStr);

        //Ten NCC
        rowNum++;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("SupplierNameMultiLanguageShortHand") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(supplierName, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("SupplierNameMultiLanguageShortHand") + ": " + supplierName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Ma NCC
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("SupplierPseudoIdMultiLanguageShortHand") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(supplierPseudoId, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("SupplierPseudoIdMultiLanguageShortHand") + ": " + supplierPseudoId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Tien te
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamCurrencyMultiLanguage") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(currencyUomId, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamCurrencyMultiLanguage") + ": " + currencyUomId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //rowData
        rowNum = 12;//start insert data row
        if(itemProductList != null && itemProductList.size() > 0){
            int index = 1;
            List<String> productIdList = itemProductList.stream().map(e -> (String)e.get("productId")).collect(Collectors.toList());
            Map productOut = eci.getService().sync().name("org.dchn.IamProductServices.get#Products").parameter("productIdList", productIdList).call();
            List<Map> productDataList = (List)productOut.get("listData");
            //Set productCategorySet = productDataList.stream().map(e -> (String)e.get("productCategoryId")).collect(Collectors.toSet());
            Set categoryIdBrowsedSet = new HashSet();//cac danh muc da duyet qua va tao XSSFRow
            for(Map productData: productDataList){
                String productCategoryId = (String)productData.get("productCategoryId");
                if(!categoryIdBrowsedSet.contains(productCategoryId)){
                    String categoryName = (String)productData.get("categoryName");
                    EntityValue productCatLanguage = eci.getEntity().find("iam.product.category.ProductCategoryGeo")
                                                        .condition("productCategoryId", productCategoryId)
                                                        .condition("geoId", this.geoId)
                                                        .condition("categoryGeoPurposeEnumId", "CgpGeoLanguage").one();
                    if(productCatLanguage != null && productCatLanguage.getString("description") != null && productCatLanguage.getString("description").trim().length() > 0){
                        categoryName = productCatLanguage.getString("description");
                    }
                    excelUtils.createRowContent(sheet, rowNum, 0, boldAlignLeftBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, firstCol, lastCol - 1), categoryName);
                    excelUtils.createRowContent(sheet, rowNum, lastCol, boldAlignLeftBorderStyle, (short)400, null, "");
                    categoryIdBrowsedSet.add(productCategoryId);
                    rowNum++;
                }//end if check categoryIdBrowsedSet.contains(productCategoryId)
                List<Map> subItems = itemProductList.stream().filter(e -> e.get("productId").equals(productData.get("productId"))).collect(Collectors.toList());
                for(Map item: subItems) {
                    //STT
                    excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
                    //Ten SP
                    String productName = (String)item.get("itemDescription");
                    String pseudoId = (String)item.get("pseudoId");

                    if(geoId != null){
                        EntityValue productGeo = eci.getEntity().find("mantle.product.ProductGeo")
                                .condition("productId", item.get("productId"))
                                .condition("geoId", this.geoId).one();
                        if(productGeo != null && productGeo.getString("description") != null && productGeo.getString("description").trim().length() > 0){
                            productName = productGeo.getString("description");
                        }
                    }
                    //Ma san pham
                    excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, null, pseudoId);
                    //Ten san pham
                    excelUtils.createRowContent(sheet, rowNum, 2, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 2, 6), productName);
                    //So luong
                    excelUtils.createRowContent(sheet, rowNum, 7, normalBorderAlignRightStyle, (short)400, null, item.get("quantity"));

                    //Don gia
                    excelUtils.createRowContent(sheet, rowNum, 8, normalBorderAlignRightStyle, CellType.NUMERIC, (short)400, null, item.get("unitAmount"));

                    //Thanh tien
                    excelUtils.createRowContent(sheet, rowNum, 9, normalBorderAlignRightStyle, CellType.NUMERIC, (short)400, null, item.get("itemTotal"));
                    rowNum++;
                    index++;
                }//end inner for
            }//end for
        }//end if check itemProductList

        rowNum++;
        //Phi FOB, phi keo Cont, phi vc duong bien
        if(itemExpenseList != null && itemExpenseList.size() > 0){
            for(Map itemExpense: itemExpenseList){
                String localizedEnum = eci.getL10n().localize("Enum" + itemExpense.get("itemTypeEnumId"));
                if(localizedEnum == null || localizedEnum.equalsIgnoreCase("Enum" + itemExpense.get("itemTypeEnumId"))){
                    localizedEnum = itemExpense.get("comments") != null? (String)itemExpense.get("comments") : (String)itemExpense.get("itemDescription");
                }
                excelUtils.createRowContent(sheet, rowNum, 6, italicAlignLeftStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 6, 7), localizedEnum);
                excelUtils.createRowContent(sheet, rowNum, 8, italicAlignRightStyle, (short)400, null, itemExpense.get("unitAmount"));
                rowNum++;
            }//end for
        }//end if

        //Tong tien
        excelUtils.createRowContent(sheet, rowNum, 6, boldAlignLeftStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 6, 7), eci.getL10n().localize("IamGrandTotalMultiLanguage"));
        excelUtils.createRowContent(sheet, rowNum, 8, boldAlignRightStyle, (short)400, null, orderHeader.get("grandTotal"));

        //ghi chu
        /*rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 1, boldAlignLeftStyle, (short)400, null, eci.getL10n().localize("IamNote"));
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignLeftStyle, (short)400, null, orderNote != null? orderNote: "");*/

        //kiem soat, nguoi nhap hang, van chuyen, chu ky
        /*rowNum+=2;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, firstCol, 3), eci.getL10n().localize("IamQualityControlParty"));
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), eci.getL10n().localize("IamAssetReceiver"));
        excelUtils.createRowContent(sheet, rowNum, 7, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), eci.getL10n().localize("IamTransporter"));*/
        //ky, ho ten
        /*rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, firstCol, 3), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 4, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 7, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");*/

        //quan ly kho, bo phan an ninh, Kh ky nhan, chu ky
        /*rowNum += 5;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, firstCol, 3), eci.getL10n().localize("IamWarehouseManager"));
        excelUtils.createRowContent(sheet, rowNum, 4, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), eci.getL10n().localize("IamSecurity"));
        excelUtils.createRowContent(sheet, rowNum, 7, boldAlignCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), eci.getL10n().localize("IamCustomerSignature"));*/
        //ky, ho ten
        /*rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, firstCol, 3), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 4, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 4, 6), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");
        excelUtils.createRowContent(sheet, rowNum, 7, italicCenterStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 7, 8), "(" + eci.getL10n().localize("IamFullNameAndSignature") + ")");*/
        return wb;
    }
}
