package org.iam.excel;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.moqui.impl.context.ExecutionContextImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class ProfitReportExcel {
    protected ExecutionContextImpl eci;
    private String timePeriodId;
    private String organizationPartyId;

    public ProfitReportExcel(ExecutionContextImpl eci, String timePeriodId, String organizationPartyId){
        this.eci = eci;
        this.timePeriodId = timePeriodId;
        this.organizationPartyId = organizationPartyId;
        if(this.timePeriodId == null){
            String currYear = eci.getL10n().format(eci.getUser().getNowCalendar().get(Calendar.YEAR), "0000");
            Date fromDate = eci.getL10n().parseDate(currYear + "-01-01", "yyyy-MM-dd");
            Map timePeriodOut = eci.getService().sync().name("mantle.party.TimeServices.getOrCreate#TimePeriod")
                    .parameter("partyId", this.organizationPartyId)
                    .parameter("timePeriodTypeId", "FiscalYear")
                    .parameter("fromDate", fromDate).call();
            this.timePeriodId = (String)timePeriodOut.get("timePeriodId");
        }
    }
    public XSSFWorkbook exportData() throws IOException {
        ExcelUtils excelUtils = new ExcelUtils();
        Map profitOut = eci.getService().sync().name("org.dchn.IamAccountingServices.get#ProfitReport")
                .parameter("timePeriodId",timePeriodId)
                .parameter("organizationPartyId",organizationPartyId).call();
        List<Map> data = (List<Map>)profitOut.get("data");
        BigDecimal profitAmount = (BigDecimal)profitOut.get("profitAmount");
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("Sheet1");
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle boldAlignCenterBorderStyle = excelUtils.getBoldAlignCenterBorderStyle(wb);
        XSSFCellStyle boldAlignLeftBorderStyle = excelUtils.getBoldAlignLeftBorderStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignLeftStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle italicBorderAlignLeftStyle = excelUtils.getItalicAlignLeftBorderStyle(wb);
        XSSFCellStyle italicBorderAlignRightStyle = excelUtils.getItalicAlignRightBorderStyle(wb);
        XSSFCellStyle normalBorderAlignCenterStyle = excelUtils.getNormalCenterFullBorderStyle(wb);

        int rowNum = 1;
        sheet.setColumnWidth(0, 1500);
        sheet.setColumnWidth(1, 19000);
        sheet.setColumnWidth(2, 7200);
        CellRangeAddress cellAddresses = new CellRangeAddress(1, 1, 0, 2);
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterStyle, (short) 650, cellAddresses, eci.getL10n().localize("IamProfitReport").toUpperCase());
        rowNum++;
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignCenterBorderStyle, (short) 400, null, eci.getL10n().localize("IamOrderShort"));
        excelUtils.createRowContent(sheet, rowNum, 1, boldAlignCenterBorderStyle, (short) 400, null, eci.getL10n().localize("IamPaymentComment"));
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignCenterBorderStyle, (short) 400, null, eci.getL10n().localize("IamAmount"));
        int index = 1;
        rowNum++;
        for(Map item: data){
            List<Map> childItems = item.get("items") != null? (List<Map>)item.get("items"): null;
            if(childItems != null && childItems.size() > 0){
                CellRangeAddress sequenceCellAddresses = new CellRangeAddress(rowNum, rowNum + childItems.size(), 0, 0);
                excelUtils.createRowContent(sheet, rowNum, 0, normalBorderAlignCenterStyle, (short) 400, sequenceCellAddresses, index);
                excelUtils.createRowContent(sheet, rowNum, 1, normalBorderAlignLeftStyle, (short) 400, null, item.get("label"));
                excelUtils.createRowContent(sheet, rowNum, 2, normalBorderAlignRightStyle, (short) 400, null, item.get("value"));
                for(Map childItem: childItems){
                    rowNum++;
                    excelUtils.createRowContent(sheet, rowNum, 1, italicBorderAlignLeftStyle, (short) 400, null, " + " + childItem.get("label"));
                    excelUtils.createRowContent(sheet, rowNum, 2, italicBorderAlignRightStyle, (short) 400, null, childItem.get("value"));
                }
            }else{
                excelUtils.createRowContent(sheet, rowNum, 0, normalBorderAlignCenterStyle, (short) 400, null, index);
                excelUtils.createRowContent(sheet, rowNum, 1, normalBorderAlignLeftStyle, (short) 400, null, item.get("label"));
                excelUtils.createRowContent(sheet, rowNum, 2, normalBorderAlignRightStyle, (short) 400, null, item.get("value"));
            }
            rowNum++;
            index++;
        }
        excelUtils.createRowContent(sheet, rowNum, 0, boldAlignLeftBorderStyle, (short) 400, null, "");
        excelUtils.createRowContent(sheet, rowNum, 1, boldAlignLeftBorderStyle, (short) 400, null, eci.getL10n().localize("IamProfit"));
        excelUtils.createRowContent(sheet, rowNum, 2, boldAlignRightBorderStyle, (short) 400, null, profitAmount);
        return wb;
    }
}
