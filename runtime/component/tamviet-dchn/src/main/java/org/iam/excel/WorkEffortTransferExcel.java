package org.iam.excel;

import org.apache.poi.xssf.usermodel.*;
import org.iam.model.ExcelRichTextInfo;
import org.moqui.entity.EntityCondition;
import org.moqui.entity.EntityList;
import org.moqui.entity.EntityValue;
import org.moqui.impl.context.ExecutionContextImpl;
import org.moqui.resource.ResourceReference;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WorkEffortTransferExcel {
    protected ExecutionContextImpl eci;
    private String workEffortId;
    private String location;

    public WorkEffortTransferExcel(ExecutionContextImpl eci, String workEffortId, String location){
        this.eci = eci;
        this.workEffortId = workEffortId;
        this.location = location;
    }

    public XSSFWorkbook exportData() throws IOException {
        ExcelUtils excelUtils = new ExcelUtils();
        EntityList wefList = eci.getEntity().find("iam.shipment.WorkEffortAndTransferShipment")
                .condition("workEffortId", workEffortId).list();
        EntityValue wef = wefList.getFirst();
        String universalId = wef.get("universalId") != null? wef.getString("universalId") : "";
        Timestamp actualStartDate = wef.getTimestamp("actualStartDate");
        String originFacilityName = wef.getString("originFacilityName");
        String destinationFacilityName = wef.getString("destinationFacilityName");
        List<String> selectFields = Arrays.asList("shipmentId","productId","pseudoId","productName","productClassEnum","quantity","quantityReceivedTotal",
                "quantityIssuedTotal","quantityAcceptedTotal","quantityTotal");
        EntityList productItems = eci.getEntity().find("iam.shipment.ShipmentTransferItemDetail")
                .condition("shipWorkEffortId", workEffortId)
                .condition("quantity", EntityCondition.GREATER_THAN, 0)
                .selectFields(selectFields)
                .list();
        ResourceReference rr = eci.getResource().getLocationReference(location);
        XSSFWorkbook wb = new XSSFWorkbook(rr.openStream());
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont normalFont = excelUtils.getNormalFont(wb);
        XSSFFont boldFont = excelUtils.getBoldFont(wb);
        XSSFCellStyle italicAlignCenterStyle = excelUtils.getItalicStyle(wb);
        XSSFCellStyle normalCenterBorderStyle = excelUtils.getNormalCenterFullBorderStyle(wb);
        XSSFCellStyle normalBorderStyle = excelUtils.getNormalFullBorderStyle(wb);
        XSSFCellStyle normalBorderAlignRightStyle = excelUtils.getNormalRightFullBorderStyle(wb);
        XSSFCellStyle boldAlignRightBorderStyle = excelUtils.getBoldAlignRightBorderStyle(wb);
        XSSFCellStyle boldAlignCenterStyle = excelUtils.getBoldAlignCenterStyle(wb);
        XSSFCellStyle italicCenterStyle = excelUtils.getItalicStyle(wb);

        //Mã phiếu dieu chuyen
        int rowNum = 6;
        List<ExcelRichTextInfo> richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamCode") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(universalId, normalFont));
        XSSFRichTextString richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamCode") + ": " + universalId, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Ngày tạo
        String transferDate = eci.getL10n().format(actualStartDate, "dd/MM/yyyy");
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamDateTime") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(transferDate, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamDateTime") + ": " + transferDate, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        //Từ kho
        rowNum++;
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamOriginalFacility") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(originFacilityName, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamOriginalFacility") + ": " + originFacilityName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 0, null, (short)400, null, richTextString);

        //Đến kho
        richTextInfos = new ArrayList<>();
        richTextInfos.add(new ExcelRichTextInfo(eci.getL10n().localize("IamDestinationFacility") + ": ", boldFont));
        richTextInfos.add(new ExcelRichTextInfo(destinationFacilityName, normalFont));
        richTextString = excelUtils.createRichTextStr(eci.getL10n().localize("IamDestinationFacility") + ": " + destinationFacilityName, richTextInfos);
        excelUtils.createRowContent(sheet, rowNum, 6, null, (short)400, null, richTextString);

        rowNum = 11;//start insert data row
        int index = 1;
        BigDecimal quantityTotal = BigDecimal.ZERO;
        for(Map item: productItems){
            //STT
            excelUtils.createRowContent(sheet, rowNum, 0, normalCenterBorderStyle, (short)400, null, index);
            //ma sp
            excelUtils.createRowContent(sheet, rowNum, 1, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 1, 2), item.get("pseudoId"));
            //ten sp
            excelUtils.createRowContent(sheet, rowNum, 3, normalBorderStyle, (short)400, excelUtils.createCellAddresses(rowNum, rowNum, 3, 7), item.get("productName"));
            //so luong
            excelUtils.createRowContent(sheet, rowNum, 8, normalBorderAlignRightStyle, (short)400, null, item.get("quantity"));
            //quantityTotal = quantityTotal.add((BigDecimal)item.get("estimatedQuantity"));
            rowNum++;
            index++;
        }
        return wb;
    }
}
