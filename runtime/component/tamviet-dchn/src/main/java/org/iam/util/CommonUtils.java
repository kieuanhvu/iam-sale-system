package org.iam.util;

import org.apache.commons.codec.binary.StringUtils;
import org.moqui.context.ArtifactExecutionInfo;
import org.moqui.impl.context.ArtifactExecutionFacadeImpl;
import org.moqui.impl.context.ArtifactExecutionInfoImpl;
import org.moqui.impl.context.ExecutionContextImpl;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class CommonUtils {
    public static Timestamp getStartOfDate(ExecutionContextImpl ec, Timestamp ts){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(ts);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getStartOfMonth(ExecutionContextImpl ec, Timestamp ts){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(ts);
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getEndOfDate(ExecutionContextImpl ec, Timestamp ts){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(ts);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getDateOffset(ExecutionContextImpl ec, Timestamp date, int offset){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, offset);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getHourOffset(ExecutionContextImpl ec, Timestamp date, int offset){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(date);
        cal.add(Calendar.HOUR, offset);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getEndOfMonth(ExecutionContextImpl ec, Timestamp ts){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setTime(ts);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        Timestamp convertTs = new Timestamp(cal.getTimeInMillis());
        return convertTs;
    }

    public static Timestamp getStartOfMonth(ExecutionContextImpl ec, int month, int year){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getEndOfMonth(ExecutionContextImpl ec, int month, int year){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.set(Calendar.MONTH, month + 1);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getStartOfWeek(ExecutionContextImpl ec, int week, int year){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.set(Calendar.YEAR, year);
        cal.setMinimalDaysInFirstWeek(7);
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getEndOfWeek(ExecutionContextImpl ec, int week, int year){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.set(Calendar.YEAR, year);
        cal.setMinimalDaysInFirstWeek(7);
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        //cal.add(Calendar.DATE, 6);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static int getCurrentWeek(ExecutionContextImpl ec){
        Calendar cal = ec.getUser().getNowCalendar();
        cal.setMinimalDaysInFirstWeek(7);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    public static String encodeString(String str){
        String value = StringUtils.newStringUtf8(str.getBytes());
        return value;
    }

    public static Locale getGlobalLocale(){
        return new Locale.Builder().setLanguage("en").setRegion("US").build();
    }

    public static TimeZone getGlobalTimeZone(){
        return TimeZone.getTimeZone("Europe/London");
    }

    public static boolean hasPermissionArtifact(ArtifactExecutionFacadeImpl aefi, String artifactName,
                                                ArtifactExecutionInfo.ArtifactType typeEnum, ArtifactExecutionInfo.AuthzAction actionEnum){
        ArtifactExecutionInfoImpl aei = new ArtifactExecutionInfoImpl(artifactName, typeEnum, actionEnum, null);
        return aefi.isPermitted(aei, null, true, false, false, null);
    }
}
