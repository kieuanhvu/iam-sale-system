<#--
This software is in the public domain under CC0 1.0 Universal plus a Grant of Patent License.

To the extent possible under law, the author(s) have dedicated all
copyright and related and neighboring rights to this software to the
public domain worldwide. This software is distributed without any
warranty.

You should have received a copy of the CC0 Public Domain Dedication
along with this software (see the LICENSE.md file). If not, see
<http://creativecommons.org/publicdomain/zero/1.0/>.
-->

<#-- See the mantle.order.OrderInfoServices.get#OrderDisplayInfo service for data preparation -->

<#assign cellPadding = "1pt">
<#assign dateFormat = "dd/MM/yyyy">
<#macro encodeText textValue>${Static["org.moqui.util.StringUtilities"].encodeForXmlAttribute(textValue!"", false)}</#macro>
<#setting locale="en_US">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="ArialUni, ArialUniBold" font-size="10pt">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="letter-portrait" page-width="8.5in" page-height="11in"
                               margin-top="0.5in" margin-bottom="0.5in" margin-left="0.5in" margin-right="0.5in">
            <fo:region-body margin-top="1.2in" margin-bottom="0.6in"/>
            <fo:region-before extent="1.2in"/>
            <fo:region-after extent="0.5in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="letter-portrait" id="mainSequence">
        <fo:static-content flow-name="xsl-region-before">
            <fo:block font-size="12pt" text-align="center" margin-bottom="0.1in">Phiếu nhập kho thành phẩm</fo:block>
            <fo:table table-layout="fixed" margin-bottom="0.1in" width="7.5in">
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding="3pt" width="1.75in">
                            <fo:block font-family="ArialUniBold">Mã phiếu</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="2in">
                            <fo:block>${universalId}</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="1.75in">
                            <fo:block font-family="ArialUniBold">Tên phiếu</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="2in">
                            <fo:block><@encodeText description!""/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="3pt" width="1.75in">
                            <fo:block font-family="ArialUniBold">Ngày nhập</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="2in">
                            <fo:block>${ec.l10n.format(actualStartDate, dateFormat)}</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="1.75in">
                            <fo:block font-family="ArialUniBold">Trạng thái</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="2in">
                            <fo:block><@encodeText status!""/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="3pt" width="1.75in">
                            <fo:block font-family="ArialUniBold">Tổ chức</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" width="2in">
                            <fo:block><@encodeText organizationPseudoId!""/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after" font-size="8pt">
            <fo:block border-top="thin solid black">
                <fo:block text-align="center">Page <fo:page-number/></fo:block>
            </fo:block>
        </fo:static-content>

        <fo:flow flow-name="xsl-region-body">
            <fo:block font-size="12pt" text-align="center" margin-bottom="0.1in" margin-top="0.1in">Thành phẩm nhập kho</fo:block>
            <fo:table table-layout="fixed" width="100%">
                <fo:table-header font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                    <fo:table-cell width="0.3in" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                        <fo:block font-family="ArialUniBold">STT</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="2in" padding="${cellPadding}" border-right="thin solid black"><fo:block font-family="ArialUniBold" padding-left="0.1in">Mã thành phẩm</fo:block></fo:table-cell>
                    <fo:table-cell width="4in" padding="${cellPadding}" border-right="thin solid black"><fo:block font-family="ArialUniBold">Tên thành phẩm</fo:block></fo:table-cell>
                    <fo:table-cell padding="${cellPadding}" border-right="thin solid black"><fo:block text-align="right" font-family="ArialUniBold">Số lượng</fo:block></fo:table-cell>
                </fo:table-header>
                <fo:table-body>
                    <#if (productList?has_content)>
                        <#list productList as prod>
                            <fo:table-row font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                                <fo:table-cell padding="${cellPadding}" text-align="center" border-right="thin solid black" border-left="thin solid black"><fo:block>${prod_index + 1}</fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black">><fo:block padding-left="0.1in">${prod.pseudoId}</fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black">><fo:block><@encodeText (prod.productName)!""/></fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black">><fo:block text-align="right">${prod.estimatedQuantity}</fo:block></fo:table-cell>
                            </fo:table-row>
                        </#list>
                    <#else>
                        <fo:table-row font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                            <fo:table-cell padding="${cellPadding}" text-align="center" border-left="thin solid black" number-columns-spanned="4" border-right="thin solid black"><fo:block>Không có dữ liệu</fo:block></fo:table-cell>
                        </fo:table-row>
                    </#if>
                </fo:table-body>
            </fo:table>

            <fo:block border-bottom-width="0.5pt" border-bottom-style="solid" margin-top="0.2in"></fo:block>
            <fo:block font-size="12pt" text-align="center" margin-bottom="0.1in" margin-top="0.2in">Nguyên vật liệu sử dụng</fo:block>
            <fo:table table-layout="fixed" width="100%">
                <fo:table-header font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                    <fo:table-cell width="0.3in" font-family="ArialUniBold" padding="${cellPadding}" border-left="thin solid black" border-right="thin solid black"><fo:block>STT</fo:block></fo:table-cell>
                    <fo:table-cell width="2in" font-family="ArialUniBold" padding="${cellPadding}" border-right="thin solid black"><fo:block padding-left="0.1in">Mã NVL</fo:block></fo:table-cell>
                    <fo:table-cell width="4in" font-family="ArialUniBold" padding="${cellPadding}" border-right="thin solid black"><fo:block>Tên NVL</fo:block></fo:table-cell>
                    <fo:table-cell padding="${cellPadding}" font-family="ArialUniBold" border-right="thin solid black"><fo:block text-align="right">Số lượng</fo:block></fo:table-cell>
                </fo:table-header>
                <fo:table-body>
                    <#if (productMaterialList?has_content)>
                        <#list productMaterialList as prod>
                            <fo:table-row font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                                <fo:table-cell padding="${cellPadding}" text-align="center" border-left="thin solid black" border-right="thin solid black"><fo:block>${prod_index + 1}</fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black"><fo:block padding-left="0.1in">${prod.pseudoId}</fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black"><fo:block><@encodeText (prod.productName)!""/></fo:block></fo:table-cell>
                                <fo:table-cell padding="${cellPadding}" border-right="thin solid black"><fo:block text-align="right">${prod.estimatedQuantity}</fo:block></fo:table-cell>
                            </fo:table-row>
                        </#list>
                    <#else>
                        <fo:table-row font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                            <fo:table-cell padding="${cellPadding}" text-align="center" border-left="thin solid black" number-columns-spanned="4" border-right="thin solid black"><fo:block>Không có dữ liệu</fo:block></fo:table-cell>
                        </fo:table-row>
                    </#if>
                </fo:table-body>
            </fo:table>
        </fo:flow>
    </fo:page-sequence>
</fo:root>