<#assign cellPadding = "1.1pt">
<#assign dateFormat = "dd/MM/yyyy">
<#macro encodeText textValue>${Static["org.moqui.util.StringUtilities"].encodeForXmlAttribute(textValue!"", false)}</#macro>

<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="ArialUni, ArialUniBold, ArialUniItalic" font-size="10pt">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="letter-portrait" page-width="8.5in" page-height="11in"
                               margin-top="0.5in" margin-bottom="0.5in" margin-left="0.3in" margin-right="0.3in">
            <fo:region-body margin-top="0.05in" margin-bottom="0.5in"/>
            <fo:region-before extent="1.2in"/>
            <fo:region-after extent="0.5in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="letter-portrait" id="mainSequence">
        <fo:static-content flow-name="xsl-region-after" font-size="8pt">
            <fo:block border-top="thin solid black">
                <fo:block text-align="center">Trang <fo:page-number/></fo:block>
            </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
            <#if logoLocation?has_content>
                <fo:block-container absolute-position="absolute" top="-0.1in" left="0in" width="2.2in">
                    <fo:block text-align="left">
                        <fo:external-graphic src="${logoLocation}" content-height="0.75in" content-width="scale-to-fit" scaling="uniform"/>
                    </fo:block>
                </fo:block-container>
            </#if>
            <fo:block font-size="14pt" font-family="ArialUniBold" text-align="center" margin-bottom="0.1in">
                CÔNG TY CỔ PHẦN QUẠT ĐIỆN CƠ HÀ NỘI
            </fo:block>
            <fo:block text-align="center" margin-bottom="0.1in">
                Nhà máy sản xuất: Ấp 6+7, Thiện Tân, Vĩnh Cửu, Đồng Nai
            </fo:block>
            <fo:table table-layout="fixed" margin-bottom="0.1in" width="7.5in">
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding="3pt" text-align="center" width="3.2in">
                            <fo:block>Kinh doanh: 0917272628</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" text-align="center" width="2in">
                            <fo:block>Kế toán: 0251.629.3868</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" text-align="center" width="2.3in">
                            <fo:block>Bảo hành: 0915772388</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>

            <fo:block font-size="15pt" text-align="center" font-family="ArialUniBold" margin-top="0.15in">LỆNH SẢN XUẤT - NGÀY ${wef.actualStartDate?string["dd/MM/yyyy"]}</fo:block>

            <fo:table table-layout="fixed" width="100%" margin-top="0.3in" font-size="9pt">
                <fo:table-header font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                    <fo:table-cell width="0.4in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                        <fo:block font-family="ArialUniBold">#</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="1.2in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Mã sản phẩm</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="4.0in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Tên sản phẩm</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="1.7in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Kho</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="0.6in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Số lượng</fo:block>
                    </fo:table-cell>
                </fo:table-header>
                <fo:table-body>
                    <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                        <fo:table-cell width="0.4in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                            <fo:block>1</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="1.2in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                            <fo:block>2</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="4.0in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block>3</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="1.7in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block>4</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="0.6in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block>5</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <#if (workEffortProducts?has_content)>
                        <#list workEffortProducts as productData>
                            <#assign productCategoryId = productData.productCategoryId/>
                            <#if !categoryIdBrowsedSet?seq_contains(productCategoryId)>
                                <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                                    <fo:table-cell padding="${cellPadding}" text-align="left" border-left="thin solid black" number-columns-spanned="5" border-right="thin solid black">
                                        <fo:block font-family="ArialUniBold">${productData.get("categoryName")}</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <#assign categoryIdBrowsedSet = categoryIdBrowsedSet + [productCategoryId] />
                            </#if>
                            <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                                <fo:table-cell width="0.4in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                                    <fo:block>${productData_index + 1}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="1.2in" text-align="left" padding="${cellPadding}" border-right="thin solid black">
                                    <fo:block>${productData.pseudoId}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="4.0in" padding="${cellPadding}" text-align="left" border-right="thin solid black">
                                    <fo:block>${productData.productName}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="1.7in" padding="${cellPadding}" text-align="left" border-right="thin solid black">
                                    <fo:block>${productData.facilityName}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="0.6in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                                    <fo:block>${ec.l10n.format(productData.estimatedQuantity, "#,##0")}</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </#list>
                    </#if>
                    <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                        <fo:table-cell width="7.3in" text-align="right" number-columns-spanned="4" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                            <fo:block font-family="ArialUniBold">Cộng</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="0.6in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                            <fo:block>${ec.l10n.format(totalQuantity, "#,##0")}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table><#--end table list data-->

            <fo:table table-layout="fixed" width="100%" margin-top="0.2in" font-size="9pt" keep-together.within-page="always">
                <fo:table-body>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block font-family="ArialUniBold">Giám đốc sản xuất</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body><#--end table signature-->
            </fo:table><#--end fo-region-body-->
        </fo:flow>
    </fo:page-sequence>
</fo:root>
