<#assign cellPadding = "1pt">
<#assign dateFormat = "dd/MM/yyyy">
<#macro encodeText textValue>${Static["org.moqui.util.StringUtilities"].encodeForXmlAttribute(textValue!"", false)}</#macro>
<#setting locale="en_US">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="ArialUni, ArialUniBold, ArialUniItalic" font-size="9pt">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="letter-landscape" page-width="8.3in" page-height="5.8in"
                               margin-top="0.1in" margin-bottom="0.1in" margin-left="0.1in" margin-right="0.1in">
            <fo:region-body margin-top="0in" margin-bottom="0in"/>
            <fo:region-before extent="0.5in"/>
            <fo:region-after extent="0in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="letter-landscape" id="mainSequence">
        <fo:flow flow-name="xsl-region-body">
            <fo:table table-layout="fixed" margin-bottom="0.15in" width="8.2in">
                <fo:table-body>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="0pt" text-align="center" width="0.7in">
                            <#if logoLocation?has_content>
                                <fo:block-container width="0.7in">
                                    <fo:block text-align="left">
                                        <fo:external-graphic src="${logoLocation}" content-width="scale-to-fit" content-height="0.5in" scaling="uniform"/>
                                    </fo:block>
                                </fo:block-container>
                            <#else>
                                <fo:block><fo:leader /></fo:block>
                            </#if>
                        </fo:table-cell>
                        <fo:table-cell padding-top="1pt" text-align="left" width="3.52in">
                            <fo:block font-size="10pt" font-family="ArialUniBold" text-align="left" margin-bottom="0.05in" margin-top="0.1in">
                                ${fromPartyName}
                            </fo:block>
                            <fo:block text-align="left" margin-bottom="0.1in">
                                <fo:inline font-style="italic">${fromPostalAddress}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="left" width="0.58in">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="left" width="3.4in">
                            <fo:block font-size="9pt" font-family="ArialUniBold" text-align="center" margin-bottom="0.07in" margin-top="0.06in">
                                Mẫu số 02 - TT
                            </fo:block>
                            <fo:block text-align="center" margin-bottom="0.1in">
                                <fo:inline>(Ban hành theo Thông tư số 200/2014/TT-BTC Ngày 22/12/2014 của Bộ Tài chính)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            <fo:table table-layout="fixed" margin-bottom="0.15in" width="8.2in">
                <fo:table-body>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" width="2.1in">
                            <fo:block><fo:leader /></fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center" width="4in">
                            <fo:block font-size="13pt" font-family="ArialUniBold" text-align="center" margin-bottom="0.1in">
                                PHIẾU CHI
                            </fo:block>
                            <fo:block text-align="center" margin-bottom="0.1in">
                                <fo:inline font-style="italic">
                                    Ngày ${effectiveDate?string["dd"]} tháng ${effectiveDate?string["MM"]} năm ${effectiveDate?string["yyyy"]}
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="left" width="2.1in">
                            <fo:block text-align="left" margin-bottom="0.05in">
                                <fo:inline>
                                    Quyển số: ${notebookNumber!"......"}
                                </fo:inline>
                            </fo:block>
                            <fo:block text-align="left" margin-bottom="0.05in">
                                <fo:inline>
                                    Số: ${paymentNumber!"......"}
                                </fo:inline>
                            </fo:block>
                            <fo:block text-align="left" margin-bottom="0.05in">
                                <fo:inline>
                                    Nợ: ${debitAccount!"......"}
                                </fo:inline>
                            </fo:block>
                            <fo:block text-align="left" margin-bottom="0.05in">
                                <fo:inline>
                                    Có: ${creditAccount!"......"}
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            <fo:table table-layout="fixed" margin-left="0.15in" margin-bottom="0.15in" width="8.1in">
                <fo:table-column column-number="1" column-width="2.6in"/>
                <fo:table-column column-number="2" column-width="5.5in"/>
                <fo:table-body>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" number-columns-spanned="2">
                            <fo:block margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Họ và tên người nhận tiền: </fo:inline>
                                <fo:inline>${toPartyName!""}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" number-columns-spanned="2">
                            <fo:block margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Địa chỉ: </fo:inline>
                                <fo:inline>${toPostalAddress}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" number-columns-spanned="2">
                            <fo:block margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Lý do chi: </fo:inline>
                                <fo:inline>${pmnt.comments!""}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" width="1.3in">
                            <fo:block width="1.3in" margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Số tiền: </fo:inline>
                                <fo:inline>${pmnt.amount?string[",##0.00"]} ${currencyUomDisplay}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="left" width="6in">
                            <fo:block width="6in" margin-bottom="0.09in"><fo:inline font-style="italic">(Viết bằng chữ): ${amountInWords!"..................."}</fo:inline></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="left" width="1.3in">
                            <fo:block width="1.3in" margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Kèm theo: </fo:inline>
                                <#if numberDocument?exists>
                                    <fo:inline>${numberDocument?string["00"]}</fo:inline>
                                </#if>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="left" width="6in">
                            <fo:block width="6in" margin-bottom="0.09in">
                                <fo:inline font-family="ArialUniBold">Chứng từ gốc:</fo:inline>
                                <#if documentName?has_content>
                                    <fo:inline>${documentName}</fo:inline>
                                </#if>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table><#--end thong tin nguoi nhan so tien-->
            <fo:table table-layout="fixed" margin-bottom="0.15in" margin-top="0.25in" width="8.1in">
                <fo:table-body>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.03in">
                                <fo:inline font-family="ArialUniBold">Giám đốc</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.03in">
                                <fo:inline font-family="ArialUniBold">Kế toán trưởng</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.03in">
                                <fo:inline font-family="ArialUniBold">Thủ quỹ</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.03in">
                                <fo:inline font-family="ArialUniBold">Người lập phiếu</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.03in">
                                <fo:inline font-family="ArialUniBold">Người nhận tiền</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="9pt">
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.05in">
                                <fo:inline font-style="italic">(Ký, họ tên)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.05in">
                                <fo:inline font-style="italic">(Ký, họ tên)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.05in">
                                <fo:inline font-style="italic">(Ký, họ tên)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.05in">
                                <fo:inline font-style="italic">(Ký, họ tên)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1pt" text-align="center">
                            <fo:block margin-bottom="0.05in">
                                <fo:inline font-style="italic">(Ký, họ tên)</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:flow>
    </fo:page-sequence>
</fo:root>