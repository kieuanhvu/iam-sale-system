<#assign cellPadding = "1.5pt">
<#assign dateFormat = "dd/MM/yyyy">
<#macro encodeText textValue>${Static["org.moqui.util.StringUtilities"].encodeForXmlAttribute(textValue!"", false)}</#macro>
<#setting locale="en_US">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="ArialUni, ArialUniBold, ArialUniItalic" font-size="9pt">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="letter-portrait" page-width="8.5in" page-height="11in"
                               margin-top="0.5in" margin-bottom="0.5in" margin-left="0.5in" margin-right="0.5in">
            <fo:region-body margin-top="0.05in" margin-bottom="0.5in"/>
            <fo:region-before extent="1.2in"/>
            <fo:region-after extent="0.5in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="letter-portrait" id="mainSequence">
        <fo:static-content flow-name="xsl-region-after" font-size="8pt">
            <fo:block border-top="thin solid black">
                <fo:block text-align="center">Page <fo:page-number/></fo:block>
            </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
            <#if logoLocation?has_content>
                <fo:block-container absolute-position="absolute" top="-0.1in" left="0in" width="2.2in">
                    <fo:block text-align="left">
                        <fo:external-graphic src="${logoLocation}" content-height="0.75in" content-width="scale-to-fit" scaling="uniform"/>
                    </fo:block>
                </fo:block-container>
            </#if>
            <fo:block font-size="14pt" font-family="ArialUniBold" text-align="center" margin-bottom="0.1in">
                ${toPartyName?upper_case}
            </fo:block>
            <fo:block text-align="center" margin-bottom="0.1in">
                Địa chỉ: ${toPostalAddress}
            </fo:block>

            <fo:block font-size="15pt" text-align="center" font-family="ArialUniBold" margin-top="0.15in">PHIẾU NHẬP KHO</fo:block>
            <fo:block text-align="center" margin-top="0.06in"><fo:inline font-style="italic">Số: ${shmt.orderDisplayId}</fo:inline></fo:block>

            <fo:table table-layout="fixed" width="100%" margin-top="0.2in">
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding="3pt" text-align="left" width="5in">
                            <fo:block>
                                <fo:inline font-family="ArialUniBold">Tên NCC:</fo:inline>
                                <fo:inline>${shmt.fromPartyName}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="3pt" text-align="left" width="2.5in">
                            <fo:block>
                                <fo:inline font-family="ArialUniBold">Ngày đặt hàng: </fo:inline>
                                <fo:inline>${shmt.entryDate?string["dd/MM/yyyy"]}</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            <#assign quantityTotal = 0/>
            <fo:table table-layout="fixed" width="100%" margin-top="0.3in" font-size="8pt">
                <fo:table-header font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                    <fo:table-cell width="0.5in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                        <fo:block font-family="ArialUniBold">#</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="1.6in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Mã sản phẩm</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="4.4in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Tên sản phẩm</fo:block>
                    </fo:table-cell>
                    <fo:table-cell width="0.9in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                        <fo:block font-family="ArialUniBold">Số lượng</fo:block>
                    </fo:table-cell>
                </fo:table-header>
                <fo:table-body>
                    <#if shipmentItems?has_content>
                        <#list shipmentItems as item>
                            <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="8pt">
                                <fo:table-cell width="0.5in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                                    <fo:block>${item_index + 1}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="2in" text-align="left" padding="${cellPadding}" border-right="thin solid black">
                                    <fo:block>${item.pseudoId}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="4.1in" padding="${cellPadding}" text-align="left" border-right="thin solid black">
                                    <fo:block>${item.productName!""}</fo:block>
                                </fo:table-cell>
                                <fo:table-cell width="0.9in" padding-top="${cellPadding}" padding-right="2pt" padding-bottom="${cellPadding}" text-align="right" border-right="thin solid black">
                                    <#assign quantityTotal = quantityTotal + item.quantity/>
                                    <fo:block>${item.quantity}</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </#list>
                    </#if>
                    <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="8pt">
                        <fo:table-cell width="5.8in" text-align="right" number-columns-spanned="3" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                            <fo:block font-family="ArialUniBold">Cộng</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="1.7in" padding-top="${cellPadding}" padding-right="2pt" padding-bottom="${cellPadding}" text-align="right" border-right="thin solid black">
                            <fo:block>${quantityTotal}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>

            <fo:table table-layout="fixed" width="100%" margin-top="0.3in" font-size="8pt" keep-together.within-page="always">
                <fo:table-body>
                    <fo:table-row font-size="8pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block font-family="ArialUniBold">Kiểm soát</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block font-family="ArialUniBold">Quản lý kho</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block font-family="ArialUniBold">Người nhập hàng</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row font-size="8pt">
                        <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                            <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:flow>
    </fo:page-sequence>
</fo:root>
