<#assign cellPadding = "1pt">
<#assign dateFormat = "dd/MM/yyyy">
<#macro encodeText textValue>${Static["org.moqui.util.StringUtilities"].encodeForXmlAttribute(textValue!"", false)}</#macro>

<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="ArialUni, ArialUniBold, ArialUniItalic" font-size="10pt">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="letter-portrait" page-width="8.5in" page-height="11in"
                               margin-top="0.5in" margin-bottom="0.5in" margin-left="0.3in" margin-right="0.3in">
            <fo:region-body margin-top="0.05in" margin-bottom="0.5in"/>
            <fo:region-before extent="1.2in"/>
            <fo:region-after extent="0.5in"/>
        </fo:simple-page-master>
    </fo:layout-master-set>
    <#list workEffortDataList as workEffortData>
        <fo:page-sequence master-reference="letter-portrait" initial-page-number="1" force-page-count="no-force">
            <fo:static-content flow-name="xsl-region-after" font-size="8pt">
                <fo:block border-top="thin solid black">
                    <fo:block text-align="center">Trang <fo:page-number/></fo:block>
                </fo:block>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
                <#assign deliveryOut = workEffortData.deliveryOut/>
                <#assign postalAddress = workEffortData.postalAddress/>
                <#assign telecomNumber = workEffortData.telecomNumber/>
                <#assign shipmentItems = workEffortData.shipmentItems/>
                <#assign productDataList = workEffortData.productDataList/>
                <#assign categoryIdBrowsedSet = workEffortData.categoryIdBrowsedSet/>
                <#assign productIdMap = workEffortData.productIdMap/>
                <#assign wef = workEffortData.wef/>

                <#if logoLocation?has_content>
                    <fo:block-container absolute-position="absolute" top="-0.1in" left="0in" width="2.2in">
                        <fo:block text-align="left">
                            <fo:external-graphic src="${logoLocation}" content-height="0.75in" content-width="scale-to-fit" scaling="uniform"/>
                        </fo:block>
                    </fo:block-container>
                </#if>
                <fo:block font-size="14pt" font-family="ArialUniBold" text-align="center" margin-bottom="0.1in">
                    CÔNG TY CỔ PHẦN QUẠT ĐIỆN CƠ HÀ NỘI
                </fo:block>
                <fo:block text-align="center" margin-bottom="0.1in">
                    Nhà máy sản xuất: Ấp 6+7, Thiện Tân, Vĩnh Cửu, Đồng Nai
                </fo:block>
                <fo:table table-layout="fixed" margin-bottom="0.1in" width="7.5in">
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell padding="3pt" text-align="center" width="3.2in">
                                <fo:block>Kinh doanh: 0917272628</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="3pt" text-align="center" width="2in">
                                <fo:block>Kế toán: 0251.629.3868</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="3pt" text-align="center" width="2.3in">
                                <fo:block>Bảo hành: 0915772388</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>

                <fo:block font-size="15pt" text-align="center" font-family="ArialUniBold" margin-top="0.15in">PHIẾU GIAO HÀNG</fo:block>
                <fo:block text-align="center" margin-top="0.06in"><fo:inline font-style="italic">(Kiêm phiếu xuất kho)</fo:inline></fo:block>
                <fo:block text-align="center" margin-top="0.06in"><fo:inline font-style="italic">Số: ${wef.universalId}</fo:inline></fo:block>

                <fo:table table-layout="fixed" width="100%" margin-top="0.2in">
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell padding="3pt" text-align="left" width="5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">Tên khách hàng:</fo:inline>
                                    <fo:inline>${wef.customerName?html}</fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="3pt" text-align="left" width="2.5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">Ngày giao hàng:</fo:inline>
                                    <#if wef.estimatedCompletionDate?has_content>
                                        <fo:inline>${wef.estimatedCompletionDate?string["dd/MM/yyyy"]}</fo:inline>
                                    </#if>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="3pt" text-align="left" width="5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">Địa chỉ:</fo:inline>
                                    <#if postalAddress?has_content>
                                        <fo:inline>${postalAddress.directions?html}</fo:inline>
                                    </#if>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="3pt" text-align="left" width="2.5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">Lái xe:</fo:inline>
                                    <#if deliveryOut.driverPartyName?has_content>
                                        <fo:inline>${deliveryOut.driverPartyName}</fo:inline>
                                    </#if>
                                    <#if deliveryOut.driverTelecomNumber?has_content>
                                        <fo:inline>(${deliveryOut.driverTelecomNumber})</fo:inline>
                                    </#if>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="3pt" text-align="left" width="5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">Điện thoại:</fo:inline>
                                    <#if telecomNumber?has_content>
                                        <fo:inline>${telecomNumber.contactNumber}</fo:inline>
                                    </#if>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="3pt" text-align="left" width="2.5in">
                                <fo:block>
                                    <fo:inline font-family="ArialUniBold">BKS:</fo:inline>
                                    <#if deliveryOut.deliverableName?has_content>
                                        <fo:inline>${deliveryOut.deliverableName}</fo:inline>
                                    </#if>
                                    <#if deliveryOut.deliverableDesc?has_content>
                                        <fo:inline>(${deliveryOut.deliverableDesc})</fo:inline>
                                    </#if>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>

                <fo:table table-layout="fixed" width="100%" margin-top="0.3in" font-size="9pt">
                    <fo:table-header font-size="8pt" border-bottom="thin solid black" border-top="thin solid black">
                        <fo:table-cell width="0.3in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                            <fo:block font-family="ArialUniBold">#</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="1.2in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                            <fo:block font-family="ArialUniBold">Mã SP</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="3.7in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                            <fo:block font-family="ArialUniBold">Tên, nhãn hiệu, quy cách sp, hàng hoá</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="0.5in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block font-family="ArialUniBold">Số lượng</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="0.9in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block font-family="ArialUniBold">Đơn giá</fo:block>
                        </fo:table-cell>
                        <fo:table-cell width="1.3in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                            <fo:block font-family="ArialUniBold">Thành tiền</fo:block>
                        </fo:table-cell>
                    </fo:table-header>
                    <fo:table-body>
                        <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                            <fo:table-cell width="0.3in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                                <fo:block>1</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.2in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                                <fo:block>2</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="3.7in" text-align="center" padding="${cellPadding}" border-right="thin solid black">
                                <fo:block>3</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="0.5in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                                <fo:block>4</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="0.9in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                                <fo:block>5</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.3in" padding="${cellPadding}" text-align="center" border-right="thin solid black">
                                <fo:block>6</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <#if (shipmentItems?has_content)>
                            <#list productDataList as productData>
                                <#assign productCategoryId = productData.productCategoryId/>
                                <#if !categoryIdBrowsedSet?seq_contains(productCategoryId)>
                                    <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                                        <fo:table-cell padding="${cellPadding}" text-align="left" border-left="thin solid black" number-columns-spanned="5" border-right="thin solid black">
                                            <fo:block font-family="ArialUniBold">${productData.get("categoryName")}</fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                    <#assign categoryIdBrowsedSet = categoryIdBrowsedSet + [productCategoryId] />
                                </#if>
                                <#assign subShipmentItems = productIdMap.get(productData.productId)/>
                                <#list subShipmentItems as item>
                                    <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                                        <fo:table-cell width="0.3in" text-align="center" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                                            <fo:block>${item_index + productData_index + 1}</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell width="1.2in" text-align="left" padding="${cellPadding}" border-right="thin solid black">
                                            <fo:block>${item.pseudoId}</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell width="3.7in" text-align="left" padding="${cellPadding}" border-right="thin solid black">
                                            <fo:block>${item.itemDescription}</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell width="0.5in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                                            <fo:block>${ec.l10n.format(item.quantity, "#,##0")}</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell width="0.9in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                                            <fo:block>${ec.l10n.format(item.unitAmount, "#,##0")}</fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell width="1.3in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                                            <#assign itemTotal = item.unitAmount * item.quantity/>
                                            <fo:block>${ec.l10n.format(itemTotal, "#,##0")}</fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </#list>
                            </#list>
                        </#if>
                        <fo:table-row border-bottom="thin solid black" border-top="thin solid black" font-size="9pt">
                            <fo:table-cell width="6.2in" text-align="right" number-columns-spanned="5" padding="${cellPadding}" border-right="thin solid black" border-left="thin solid black">
                                <fo:block font-family="ArialUniBold">Cộng</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.7in" padding="${cellPadding}" text-align="right" border-right="thin solid black">
                                <fo:block>${ec.l10n.format(wef.grandTotal, "#,##0")}</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>

                <fo:table table-layout="fixed" width="100%" margin-top="0.1in" margin-bottom="0.4in" font-size="9pt" keep-together.within-page="always">
                    <fo:table-body>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="4.7in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.3in" text-align="left" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">Phí vận chuyển:</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.9in" text-align="right" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">${ec.l10n.format(wef.shippingFee, "#,##0")}</fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="4.7in" text-align="center" padding="${cellPadding}">
                                <fo:block></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.3in" text-align="left" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">Chi phí khác:</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.9in" text-align="right" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">${ec.l10n.format(wef.otherFee, "#,##0")}</fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="4.7in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.3in" text-align="left" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold"><fo:inline>Tổng cộng:</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="1.9in" text-align="right" padding="${cellPadding}">
                                <#assign grandTotal = wef.grandTotal + wef.shippingFee + wef.otherFee/>
                                <fo:block font-family="ArialUniBold"><fo:inline>${ec.l10n.format(grandTotal, "#,##0")}</fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <#if wef.otherFeeDesc?has_content>
                            <fo:table-row font-size="9pt">
                                <fo:table-cell number-columns-spanned="3" text-align="left" padding="${cellPadding}">
                                    <fo:block>
                                        <fo:inline font-family="ArialUniBold">Ghi chú:</fo:inline>
                                        <fo:inline>${wef.otherFeeDesc!""}</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </#if>
                    </fo:table-body>
                </fo:table>

                <fo:table table-layout="fixed" width="100%" font-size="9pt" keep-together.within-page="always">
                    <fo:table-body>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Kiểm soát</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Người xuất hàng</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Vận chuyển</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Quản lý kho</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Bộ phận an ninh</fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block font-family="ArialUniBold">Khách hàng ký nhận</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:inline font-style="italic">(Ký, họ tên)</fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row font-size="9pt">
                            <fo:table-cell width="3in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                            <fo:table-cell width="2.5in" text-align="center" padding="${cellPadding}">
                                <fo:block><fo:leader /></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:flow><#--end fo-region-body-->
        </fo:page-sequence>
    </#list>
</fo:root>
